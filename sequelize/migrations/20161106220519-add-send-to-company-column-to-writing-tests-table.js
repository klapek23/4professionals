'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
      'writing_tests',
      'send_to_company',
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn(
      'writing_tests',
      'send_to_company'
    );
  }
};
