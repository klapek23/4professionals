'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var migrations = [],
        addStartedAtColumn = queryInterface.addColumn(
          'writing_tests',
          'startedAt',
          {
            type: Sequelize.DATE,
            defaultValue: null,
            allowNull: true
          }
        ),
        addActiveToColumn = queryInterface.addColumn(
          'writing_tests',
          'activeTo',
          {
            type: Sequelize.DATE,
            defaultValue: null,
            allowNull: true
          }
        );

    migrations.push(addStartedAtColumn);
    migrations.push(addActiveToColumn);

    return Promise.all(migrations);
  },

  down: function (queryInterface, Sequelize) {
    var migrations = [],
      removeStartedAtColumn = queryInterface.removeColumn(
        'writing_tests',
        'startedAt'
      ),
      removeActiveToColumn = queryInterface.removeColumn(
        'writing_tests',
        'activeTo'
      );

    migrations.push(removeStartedAtColumn);
    migrations.push(removeActiveToColumn);

    return Promise.all(migrations);
  }
};
