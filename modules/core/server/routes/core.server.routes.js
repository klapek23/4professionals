'use strict';

module.exports = function(app) {
  // Root routing
  var core = require('../controllers/core.server.controller'),
      user = require('../../../users/server/controllers/user.server.controller');

  app.route('/api/session').get(core.returnSession);

  // Define error pages
  app.route('/server-error').get(core.renderServerError);

  // Return a 404 for all undefined api, module or lib routes
  app.route('/:url(api|modules|lib)/*').get(core.renderNotFound);
  
  //app.route('/authentication/signin').get(core.renderLoginLayout);
  
  // Define writing tests fill route
  app.route('/writing_tests/fill/:token').get(core.renderPublicLayout);
  app.route('/writing_tests/fill/success/:token').get(core.renderPublicLayout);
  
  // Define application route
  app.route('/*').get(core.renderIndex);

  //Submit contact form data
  //app.route('/api/contact').post(core.contact);
};
