'use strict';

angular.module('core')
  .filter('leadingZero', function() {
    return function(input) {
      if (input < 10) { 
           input = '0' + input;
       }
       return input;
     };
  });