'use strict';

angular.module('core').controller('MainMenuController', ['$rootScope', '$scope', '$location', '$state', '$timeout', 'Authentication', 'Menus',
  function($rootScope, $scope, $location, $state, $timeout, Authentication, Menus) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function() {
      $timeout(function() {
        //closeMenu();
        
        function closeMenu() {
          var $menu   = angular.element('.sidebar-main-menu'),
              $button = angular.element($menu).find('.close-sidebar-menu'),
              $body   = angular.element('body');
            
          if(!$button.hasClass('menu-collapsed')) {
            $button.addClass('menu-collapsed');
          }
          if(!$menu.hasClass('collapsed')) {
            $menu.addClass('collapsed');
          }
          $menu.find('.dropdown.opened').removeClass('opened');
          if(!$body.hasClass('menu-collapsed')) {
            $body.addClass('menu-collapsed');
          }
        }
      });
      ga('send', 'pageview', $location.path());
    });

  }
]);