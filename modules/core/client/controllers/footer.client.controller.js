'use strict';

angular.module('core').controller('FooterController', ['$rootScope', '$scope', '$location', '$state', 'Authentication', 'Menus',
  function($rootScope, $scope, $location, $state, Authentication, Menus) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    // Get the footer menu
    //$scope.menu = Menus.getMenu('footer');
    
    angular.element(document).ready( function() {
      var $footer = angular.element('footer'),
          $window = angular.element(window);
          
      $footer.css({
        marginTop: - $footer.innerHeight()
      });
      
      $window.resize(function() {
        $footer.css({
          marginTop: - $footer.innerHeight()
        });
      });
    });
  }
]);