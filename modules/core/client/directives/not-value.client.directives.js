"use strict";

angular.module('core')
.directive("notValue", function(){
  // requires an isloated model
  return {
   restrict: 'A',
   require: 'ngModel',
   link: function(scope, ele, attrs, ctrl) {
     var errorValue = attrs.notValue;
     
      ctrl.$parsers.unshift(function(value) {
        var valid;
        
        if(value){
          valid = (errorValue === value ? false : true);
          ctrl.$setValidity('notValue', valid);
        }

        return valid ? value : undefined;
      });
      
      ctrl.$formatters.unshift(function(value) {
        var valid;
        
        if(value){
          valid = (errorValue === value ? false : true);
          ctrl.$setValidity('notValue', valid);
        }

        return valid ? value : undefined;
      });
   }
  };
});