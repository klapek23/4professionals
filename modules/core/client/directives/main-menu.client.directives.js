'use strict';

angular.module('core')
  .directive('mainMenu', ['$window', '$timeout', function($window, $timeout) {
    return {
      restrict: 'A',
      link: function (scope, el, attrs) {
        var $menu   = el,
            $button = angular.element($menu).find('.close-sidebar-menu'),
            toggled = true;
    
        var toggleSidebarMenu = function() {          
          $button.toggleClass('menu-collapsed');
          $menu.toggleClass('collapsed');
          angular.element('body').toggleClass('menu-collapsed');
          if(!toggled) {
            $menu.find('.submenu.opened').removeClass('opened');
          }
          toggled = !toggled;
        };
        
        $timeout(function() {
          angular.element($menu).find('ul li .submenu-toggler').on('click', function(e) {
            var $submenuToggler = angular.element(e.currentTarget),
                $submenu        = $submenuToggler.next('ul');
                
            $submenuToggler.toggleClass('active');
            $submenu.toggleClass('opened');
            
            if(toggled) {
              toggleSidebarMenu();
            }
          });
          
          $button.on('click', function() {
            toggleSidebarMenu();
          });
          
          angular.element('.body-overlay').on('click', function() {
            toggleSidebarMenu();
          });
          
        });

      }
    };
  }]);