'use strict';

angular.module('core')
  .directive('formSpinner', ['$window', '$timeout', '$compile', function($window, $timeout, $compile) {
    return {
      require: '^form',
      restrict: 'A',
      link: function ($scope, $el, $attrs, $ctrl) {
        $el.css({position: 'relative'});
        var spinner = $compile('<div class="form-spinner" ng-if="showSpinner"><div class="cp-spinner cp-bubble"></div></div>')($scope);
        $el.append(spinner);
      }
    };
  }]);