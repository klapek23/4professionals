'use strict';

angular.module('core')
  .directive('sessionDowntime', ['$window', '$timeout', '$interval', '$filter', 'Session', 'Authentication', 'moment', function($window, $timeout, $interval, $filter, Session, Authentication, moment) {
    return {
      restrict: 'E',
      templateUrl: '/modules/core/client/views/directives/session-downtime.html',
      link: function (scope, el, attrs) {
        scope.session = Session.get();

        scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
          scope.authentication = Authentication;
          scope.loggedIn = (scope.authentication.hasOwnProperty('user') && scope.authentication.user.hasOwnProperty('id') ? true : false);

          Session.get().$promise.then(function(data) {
            scope.session = data;
            scope.expires = moment(scope.session.cookie.expires).format('YYYY-MM-DD HH:mm:ss');

            scope.timeLeft = scope.getTimeRemaining(scope.expires);
          }, function(err) {
            console.log(err);
          });
        });

        scope.getTimeRemaining = function(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor( (t/1000) % 60 );
          var minutes = Math.floor( (t/1000/60) % 60 );
          var hours = Math.floor( (t/(1000*60*60)) % 24 );
          var days = Math.floor( t/(1000*60*60*24) );
          return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,
            'display': hours + ':' + ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2)
          };
        };

        scope.initializeClock = function(endtime) {
          scope.timeinterval = $interval(function() {
            scope.timeLeft = scope.getTimeRemaining(endtime);
            el.addClass('active');
            if(scope.timeLeft.total<=0){
              $interval.cancel(scope.timeinterval);
              window.location.href = '/authentication/signin';
            }
          },1000);
        };

        scope.session.$promise.then(function(data) {
          scope.session = data;
          scope.expires = moment(scope.session.cookie.expires).format('YYYY-MM-DD HH:mm:ss');

          scope.authentication = Authentication;
          scope.loggedIn = (scope.authentication.hasOwnProperty('user') && scope.authentication.user.hasOwnProperty('id') ? true : false);

          //start counter
          scope.initializeClock(scope.expires);
        }, function(err) {
          scope.session = null;
        });
      }
    };
  }]);

