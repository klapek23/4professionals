'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider', '$httpProvider',
  function($locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

    $httpProvider.interceptors.push('authInterceptor');
  }
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(function($rootScope, $state, Authentication, ClientPermissions, notify, moment, Socket) {

  // Check authentication before changing state
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    var notRequiredLoginStates = [
      'authentication.signin',
      'authentication.signup',
      'password.forgot',
      'password.reset.form',
      'forbidden',
      'not-found',
      'writing_tests.fill',
      'writing_tests.fill_success'
    ];
    
    //configure notifications
    notify.config({
      startTop: 90,
      verticalSpacing: 20,
      maximumOpen: 3
    });

    //moment.tz.setDefault("Europe/Warsaw");

    moment.locale('pl', {
      week : {
        dow : 1 // Monday is the first day of the week
      }
    });
    
    if (toState.data && toState.data.roles && toState.data.roles.length > 0) {
      var allowed = false;
      
      if (Authentication.user.roles) {
        toState.data.roles.forEach(function(role) {
          if (Authentication.user.roles !== undefined && Authentication.user.roles.indexOf(role) !== -1) {
            allowed = true;
            return true;
          }
        });
        
        if(['speaking_tests', 'writing_tests'].indexOf(toState.name.split('.')[0]) !== -1 && Authentication.user.hasOwnProperty('client') && Authentication.user.client !== null && Authentication.user.client !== undefined) {
          if(Authentication.user.client.hasOwnProperty('id')) {
            ClientPermissions.getByClientId(Authentication.user.client.id).then(function(response) {
              if(!response[toState.name.split('.')[0]] || typeof response[toState.name.split('.')[0]] === undefined) {
                event.preventDefault();
                if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
                  $state.go('forbidden');
                } else {
                  $state.go('authentication.signin');
                }
              }
            });
          } else if (!allowed) {
            event.preventDefault();
            if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
              $state.go('forbidden');
            } else {
              $state.go('authentication.signin');
            }
          }
        } else {
          if (!allowed) {
            event.preventDefault();
            if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
              $state.go('forbidden');
            } else {
              $state.go('authentication.signin');
            }
          }
        }
      } else {
        if (!allowed) {
          event.preventDefault();
          if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
            $state.go('forbidden');
          } else {
            $state.go('authentication.signin');
          }
        }
      }
    } else if(notRequiredLoginStates.indexOf(toState.name) === -1) {
      event.preventDefault();
      if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
        $state.go('forbidden');
      } else {
        $state.go('authentication.signin');
      }
    }
  });
  
  // Record previous state
  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    
    if(toState.name.split('.')[0] === 'authentication' || toState.name.split('.')[0] === 'password') {
      angular.element('body').addClass('homePage');
    } else {
      angular.element('body').removeClass('homePage');
    }
    
    if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
      angular.element('body').addClass('logged-in');
    } else {
      angular.element('body').removeClass('logged-in');
    }
    
    if (!fromState.data || !fromState.data.ignoreState) {
      $state.previous = {
        state: fromState,
        params: fromParams,
        href: $state.href(fromState, fromParams)
      };
    }
  });
});

//Then define the init function for starting up the application
angular.element(document).ready(function() {
  //Fixing facebook bug with redirect
  if (window.location.hash && window.location.hash === '#_=_') {
    if (window.history && history.pushState) {
      window.history.pushState('', document.title, window.location.pathname);
    } else {
      // Prevent scrolling by storing the page's current scroll offset
      var scroll = {
        top: document.body.scrollTop,
        left: document.body.scrollLeft
      };
      window.location.hash = '';
      // Restore the scroll offset, should be flicker free
      document.body.scrollTop = scroll.top;
      document.body.scrollLeft = scroll.left;
    }
  }

  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
