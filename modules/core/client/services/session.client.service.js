"use strict";

angular.module('core').factory('Session', ['$resource',
  function($resource) {
    return $resource('api/session', {}, {
      get: {
        method: 'GET'
      }
    });
  }
]);