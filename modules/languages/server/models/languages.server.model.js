"use strict";

module.exports = function(sequelize, DataTypes) {

  var Language = sequelize.define('language', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: "Language title must be between 1 and 250 characters in length"
        },
      }
    },
    content: DataTypes.TEXT
  }, {
    associate: function(models) {
      Language.hasMany(models.speaking_test);
      Language.belongsToMany(models.trainer,{through: 'trainers_languages'});
      Language.hasMany(models.writing_test_pattern);
      Language.hasMany(models.writing_test);
    }
  });
  return Language; 
};