'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  languagesPolicy = require('../policies/languages.server.policy'),
  languages = require(path.resolve('./modules/languages/server/controllers/languages.server.controller'));


module.exports = function(app) {

  // Speaking tests collection routes
  app.route('/api/languages')
    .all(languagesPolicy.isAllowed)
    .get(languages.list)
    .post(languages.create);

  // Single speaking tests routes
  app.route('/api/languages/:languageId')
    .all(languagesPolicy.isAllowed)
    .get(languages.read)
    .put(languages.update)
    .delete(languages.delete);

  // Finish by binding the article middleware
  app.param('languageId', languages.languageByID);

};