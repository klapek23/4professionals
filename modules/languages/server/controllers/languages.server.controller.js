'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  Language = db.language;

/**
 * Create a language
 */
exports.create = function(req, res) {

  req.body.userId = req.user.id;

  Language.create(req.body).then(function(language) {
    if (!language) {
      return res.send('users/signin', {
        errors: 'Could not create the article'
      });
    } else {
      return res.jsonp(language);
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Show the current language
 */
exports.read = function(req, res) {
  res.json(req.language);
};

/**
 * Update a language
 */
exports.update = function(req, res) {
  var language = req.language;

  language.updateAttributes({
    title: req.body.title,
    content: req.body.content
  }).then(function(language) {
    res.json(language);
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};

/**
 * Delete an language
 */
exports.delete = function(req, res) {
  var language = req.language;

  // Find the language
  Language.findById(language.id).then(function(language) {
    if (language) {

      // Delete the language
      language.destroy().then(function() {
        return res.json(language);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the language'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * List of languages
 */
exports.list = function(req, res) {
  Language.findAll()
    .then(function(languages) {
    if (!languages) {
      return res.status(404).send({
        message: 'No languages found'
      });
    } else {
      res.json(languages);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};

/**
 * Languages middleware
 */
exports.languageByID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Language is invalid'
    });
  }

  Language.findOne({
    where: {
      id: id
    }
  }).then(function(language) {
    if (!language) {
      return res.status(404).send({
        message: 'No language with that identifier has been found'
      });
    } else {
      req.language = language;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};