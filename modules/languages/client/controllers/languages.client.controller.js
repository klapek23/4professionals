'use strict';

// Languages controller
angular.module('languages').controller('LanguagesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Languages', 'notify',
  function($scope, $stateParams, $location, Authentication, Languages, notify) {
    $scope.authentication = Authentication;
    $scope.showSpinner = false;

    // Create new language
    $scope.create = function(isValid) {

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'languagesForm');

        return false;
      }

      $scope.showSpinner = true;

      // Create new language object
      var language = new Languages({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      language.$save(function(response) {
        $scope.showSpinner = false;
        $location.path('languages');
        notify({ classes: 'alert-success', message: 'New language has been added' });

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };

    // Update existing language
    $scope.update = function(isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'languagesForm');
        return false;
      }

      $scope.showSpinner = true;

      var language = $scope.language;

      language.$update(function() {
        $scope.showSpinner = false;
        $location.path('languages');
        notify({ classes: 'alert-success', message: 'Language has been updated' });
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };

    // Find existing language
    $scope.findOne = function() {
      $scope.language = Languages.get({
        languageId: $stateParams.languageId
      });
    };
  }
]);