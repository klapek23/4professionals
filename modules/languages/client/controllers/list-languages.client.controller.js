'use strict';

// ListLanguagesController
angular.module('languages').controller('ListLanguagesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Languages', '$state', '$filter', 'notify', function($scope, $stateParams, $location, Authentication, Languages, $state, $filter, notify) {
    $scope.authentication = Authentication;
    $scope.sortBy = 'title';
    $scope.sortReverse = false;

    // Find a list of languages
    $scope.languages = Languages.query(function() {
      $scope.buildPager();
    });
    
    // Remove existing Language
    $scope.remove = function(language) {
      if (language) {

        language.$remove();
        if($location.$$path !== '/languages') {
          $location.path('languages');
        } else {
          $state.reload();
        }
        notify({ classes: 'alert-success', message: 'Language has been removed' });
      } else {
        $scope.language.$remove(function() {
          if($location.$$path !== '/languages') {
          $location.path('languages');
        } else {
          $state.reload();
        }
        notify({ classes: 'alert-success', message: 'Language has been removed' });
        });
      }
    };
    
    $scope.buildPager = function() {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 10;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function() {
      $scope.filteredItems = $filter('filter')($scope.languages, {
        $: $scope.search
      });
      
      if($scope.sortBy) {
        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortBy, $scope.sortReverse);
      }
      
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.sortTable = function(sortBy) {
      if(sortBy === $scope.sortBy) {
        $scope.sortReverse = !$scope.sortReverse;
      } else {
        $scope.sortReverse = false;
      }
      
      $scope.sortBy = sortBy;
      
      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortBy, $scope.sortReverse);
      
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function() {
      $scope.figureOutItemsToDisplay();
    };
  }
]);