'use strict';

// Languages service used for communicating with the users REST endpoint
angular.module('languages').factory('Languages', ['$resource',
  function($resource) {
    return $resource('api/languages/:languageId', {
      languageId: '@id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);