'use strict';

// Setting up route
angular.module('languages').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
      .state('languages', {
        url: '/languages',
        abstract: true,
        template: '<ui-view class="">',
        data: {
          roles: ['admin', 'trainer', 'client']
        }
      })
      .state('languages.list', {
        url: '',
        templateUrl: 'modules/languages/client/views/list-languages.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('languages.create', {
        url: '/create',
        templateUrl: 'modules/languages/client/views/create-languages.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('languages.edit', {
        url: '/:languageId/edit',
        templateUrl: 'modules/languages/client/views/edit-languages.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('languages.view', {
        url: '/:languageId',
        templateUrl: 'modules/languages/client/views/view-languages.client.view.html',
        data: {
          roles: ['admin']
        }
      });
  }
]);