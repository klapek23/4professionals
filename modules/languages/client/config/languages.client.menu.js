'use strict';

// Configuring the Languages module
angular.module('languages').run(['Menus',
  function(Menus) {

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage languages',
      state: 'languages.list',
      roles: ['admin']
    });
  }
]);