'use strict';

// Create the chat configuration
module.exports = function(io, socket) {
  
  socket.on('trainersAvailabilityChange', function(message) {
    
    // Emit the event
    io.emit('trainersAvailabilityChange', {
      text: 'Trainer availability update',
      data: message
    });
  });

};