"use strict";

module.exports = function(sequelize, DataTypes) {

  var Speaking_test = sequelize.define('speaking_test', {
    status: {
      type: DataTypes.ENUM('pending', 'done', 'N/A', 'canceled'),
      defaultValue: 'pending'
    },
    level: {
      type: DataTypes.ENUM('A1', 'A1 25%', 'A1 50%', 'A1 75%', 'A2', 'A2 25%', 'A2 50%', 'A2 75%', 'B1', 'B1 25%', 'B1 50%', 'B1 75%', 'B2', 'B2 25%', 'B2 50%', 'B2 75%', 'C1', 'C1 25%', 'C1 50%', 'C1 75%', 'C2')
    },
    startsAt: {
      type: DataTypes.DATE,
      validate: {
        notEmpty: true
      }
    },
    endsAt: {
      type: DataTypes.DATE,
      validate: {
        notEmpty: true
      }
    },
    notes: DataTypes.TEXT
  }, {
    associate: function(models) {
      Speaking_test.belongsTo(models.trainer);
      Speaking_test.belongsTo(models.client);
      Speaking_test.belongsTo(models.company);
      Speaking_test.belongsTo(models.language);
      Speaking_test.hasOne(models.speaking_test_result);
      Speaking_test.belongsTo(models.candidate);
    }
  });
  return Speaking_test; 
};