"use strict";

module.exports = function(sequelize, DataTypes) {

  var Speaking_testResult = sequelize.define('speaking_test_result', {
    result_file: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    mime: {
      type: DataTypes.STRING
    },
    destination: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    size: {
      type: DataTypes.STRING
    },
    notes: DataTypes.TEXT
  }, {
    associate: function(models) {
      Speaking_testResult.belongsTo(models.speaking_test);
    }
  });
  return Speaking_testResult;
};