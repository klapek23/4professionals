'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  speaking_testsPolicy = require('../policies/speaking_tests.server.policy'),
  speaking_testsResultsPolicy = require('../policies/speaking_tests.results.server.policy'),
  speaking_testsResults = require(path.resolve('./modules/speaking_tests/server/controllers/speaking_tests.results.server.controller')),
  speaking_tests = require(path.resolve('./modules/speaking_tests/server/controllers/speaking_tests.server.controller')),
  multer = require('multer'),
  upload = multer({ dest: 'public/uploads/speaking_tests/results/' });


module.exports = function(app) {

  // Speaking tests collection routes
  app.route('/api/speaking_tests')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.list)
    .post(speaking_tests.create);

  // Single speaking tests routes
  app.route('/api/speaking_tests/:testId')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.read)
    .put(speaking_tests.update)
    .delete(speaking_tests.delete);
    
  app.route('/api/speaking_tests/running-tests/get')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.listRunningTests);
    
  app.route('/api/speaking_tests/get/by-company/:companyId')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.getTestsByCompany);
    
  app.route('/api/speaking_tests/get/by-client/:clientId')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.getTestsByClient);
    
  app.route('/api/speaking_tests/get/by-trainer/:trainerId')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.getTestsByTrainer);
    
  app.route('/api/speaking_tests/get/by-date-trainer')
    .all(speaking_testsPolicy.isAllowed)
    .get(speaking_tests.getTestsByDateTrainer);
    
  app.route('/api/speaking_tests/results/test/:testId')
    .all(speaking_testsResultsPolicy.isAllowed)
    .get(speaking_testsResults.downloadResult)
    .post(multer({storage: speaking_testsResults.speakingTestsResultMulterStorage}).single('speakingTestResult'), speaking_testsResults.changeTestResult);
    
  // Finish by binding the article middleware
  app.param('testId', speaking_tests.speaking_testByID);

};