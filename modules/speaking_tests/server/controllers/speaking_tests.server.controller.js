'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  Test = db.speaking_test,
  Candidate = db.candidate,
  config = require(path.resolve('./config/config')),
  moment = require('moment'),
  nodemailer = require('nodemailer'),
  async = require('async'),
  smtpTransport = nodemailer.createTransport(config.mailer.options);

/**
 * Create a test
 */
exports.create = function(req, res) {

  req.body.userId = req.user.id;

  async.waterfall([
    function(callback) {
      Test.findAll({
        where: {
          trainerId: req.body.trainerId,
          startsAt: moment(req.body.startsAt).format('YYYY-MM-DD HH:mm'),
          status: {
            $not: 'canceled'
          }
        }
      }).then(function(testsList) {
        if(testsList && testsList.length > 0) {
          var err = new Error('Trainer is busy at this time, please choose another one');
          callback(err);
        } else {
          callback(null);
        }
      }, function(err) {
        callback(err);
      });
    },
    function(callback) {
      Test.create(req.body).then(function(test) {
        callback(null, test);
      }, function(err) {
        callback(err);
      });
    },
    function(test, callback) {
      Candidate.findOne({
        where: {
          name: req.body.candidateFullName
        }
      }).then(function(candidate) {
        return Candidate.create({
          name: req.body.candidateFullName,
          phone: req.body.candidatePhone
        }).then(function(candidate) {
          callback(null, test, candidate);
        }, function(err) {
          callback(err);
        });
      }, function(err) {
        callback(err);
      });
    },
    function(test, candidate, callback) {
      candidate.addSpeaking_test(test).then(function() {
        callback(null, test, candidate);
      }, function(err) {
        callback(err);
      });
    },
    function(test, candidate, callback) {
      test.getLanguage().then(function(lang) {
        test.language = lang;
        
        callback(null, test, candidate);
      }, function(err) {
        callback(err);
      });
    },
    function(test, candidate, callback) {
      db.trainer.findOne({
        where: {
          id: test.trainerId
        },
        include: [db.user]
      }).then(function(trainer) {
        test.trainer = trainer;
        
        callback(null, test, candidate);
      }, function(err) {
        callback(err);
      });
    },
    function(test, candidate, callback) {
      db.client.findOne({
        where: {
          id: test.clientId
        },
        include: [db.user, db.company],
      }).then(function(client) {
        test.client = client;
        
        callback(null, test, candidate);
      }, function(err) {
        callback(err);
      });
    },
    function(test, candidate, callback) {
      res.render(path.resolve('modules/speaking_tests/server/templates/create-speaking-test-confirmation-email'), {
        appName: config.app.title,
        trainer: test.trainer.user.displayName,
        client: test.client.user.username,
        company: test.client.company.name,
        candidateFullName: candidate.name,
        candidatePhone: candidate.phone,
        language: test.language.title,
        notes: test.notes,
        startsAt: moment(test.startsAt).format('YYYY-MM-DD HH:mm'),
        endsAt: moment(test.endsAt).format('YYYY-MM-DD HH:mm'),
        url: config.app.mainUrl
      }, function(err, emailHTML) {
        if(err) {
          callback(err);
        } else {
          var mailOptions = {
            to: test.trainer.user.email,
            from: config.mailer.from,
            subject: 'You got new speaking assessment order',
            html: emailHTML
          };
          
          smtpTransport.sendMail(mailOptions, function(err) {
            if (!err) {
              callback(null, test);
            } else {
              callback(err);
            }
          });
        }
      });
    }
  ], function(err, test) {    
    if(err) {
      return res.status(400).send({
        message: err.message
      });
    } else {
      return res.json(test);
    }
  });
};

/**
 * Show the current test
 */
exports.read = function(req, res) {
  res.json(req.test);
};

/**
 * Update a test
 */
exports.update = function(req, res) {
  var test = req.test;
  
  async.waterfall([
    function(callback) {
      test.updateAttributes({
        status: req.body.status,
        level: (req.body.level ? req.body.level : null),
        startsAt: req.body.startsAt,
        endsAt: req.body.endsAt,
        notes: req.body.notes,
        languageId: req.body.language.id,
        trainerId: req.body.trainer.id,
        clientId: req.body.clientId,
        companyId: req.body.companyId
      }).then(function(test) {
        callback(null, test);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(test, callback) {
      Candidate.findOne({
        where: {
          name: test.candidate.id
        }
      }).then(function(candidate) {
        callback(null, candidate, test);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(candidate, test, callback) {
      if(candidate && candidate !== null && candidate !== undefined) {
        test.setCandidate(candidate).then(function() {
          return candidate.updateAttributes({
            phone: req.body.candidatePhone
            //email: req.body.candidateEmail
          }).then(function(candidate) {
            callback(null, candidate, test);
          }).catch(function(err) {
            callback(err);
          });
        }).catch(function(err) {
          callback(err);
        });
      } else {
        Candidate.create({
          name: req.body.candidateFullName,
          phone: req.body.candidatePhone
        }).then(function(candidate) {
          return test.setCandidate(candidate).then(function() {
            callback(null, candidate, test);
          }).catch(function(err) {
            callback(err);
          });
        }).catch(function(err) {
          callback(err);
        });
      }
    },
    function(candidate, test, callback) {
      if(test.status === 'canceled' && (test.trainer.user.email && test.trainer.user.email !== null)) {
        res.render(path.resolve('modules/speaking_tests/server/templates/cancel-speaking-test-trainer-confirmation-email'), {
          appName: config.app.title,
          trainer: test.trainer.user.displayName,
          client: test.client.user.username,
          company: test.client.company.name,
          candidateFullName: candidate.name,
          candidatePhone: candidate.phone,
          language: test.language.title,
          notes: test.notes,
          startsAt: moment(test.startsAt).format('YYYY-MM-DD HH:mm'),
          endsAt: moment(test.endsAt).format('YYYY-MM-DD HH:mm'),
          url: config.app.mainUrl
        }, function(err, emailHTML) {
          if(err) {
            callback(err);
          } else {
            var mailOptions = {
              to: test.trainer.user.email,
              from: config.mailer.from,
              subject: 'Speaking test has been canceled',
              html: emailHTML
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              if (!err) {
                callback(null, test);
              } else {
                callback(err);
              }
            });
          }
        });
      } else if(test.status === 'done' && (test.trainer.user.email && test.trainer.user.email !== null)) {
        res.render(path.resolve('modules/speaking_tests/server/templates/rate-speaking-test-client-confirmation-email'), {
          appName: config.app.title,
          trainer: test.trainer.user.displayName,
          client: test.client.user.username,
          company: test.client.company.name,
          candidateFullName: candidate.name,
          candidatePhone: candidate.phone,
          language: test.language.title,
          notes: test.notes,
          startsAt: moment(test.startsAt).format('YYYY-MM-DD HH:mm'),
          endsAt: moment(test.endsAt).format('YYYY-MM-DD HH:mm'),
          testUrl: config.app.mainUrl + '/speaking_tests/' + test.id,
          url: config.app.mainUrl
        }, function(err, emailHTML) {
          if(err) {
            callback(err);
          } else {
            var mailOptions = {
              to: test.client.user.email,
              from: config.mailer.from,
              subject: 'Speaking test has been rated',
              html: emailHTML
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              if (!err) {
                callback(null, test);
              } else {
                callback(err);
              }
            });
          }
        });
      } else {
        callback(null, test);
      }
    }
  ], function(err, test) {    
    if(err) {
      return res.status(400).send({
        message: err.message
      });
    } else {
      return res.json(test);
    }
  });
};

/**
 * Delete an test
 */
exports.delete = function(req, res) {
  var test = req.test;

  // Find the test
  Test.findById(test.id).then(function(test) {
    if (test) {

      // Delete the test
      return test.destroy().then(function() {
        return res.json(test);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the article'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};


/**
 * Cancel an test
 */
exports.cancel = function(req, res) {
  var test = req.test;

  // Find the test
  Test.findById(test.id).then(function(test) {
    if (test) {

      // Delete the test
      return test.updateAttributes({
        status: 'canceled'
      }).then(function() {
        return res.json(test);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find test'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};


/**
 * List of tests
 */
exports.list = function(req, res) {
  Test.findAll({
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.speaking_test_result, db.candidate
    ]
  }).then(function(tests) {
    if (!tests) {
      return res.status(404).send({
        message: 'No tests found'
      });
    } else {
      res.json(tests);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};


/**
 * List of tests
 */
exports.listRunningTests = function(req, res) {
  Test.findAll({
    where: {
      startsAt: {
        $gte: new Date()
      }
    },
    include: [
      db.language, db.speaking_test_result, db.candidate,
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
    ]
  }).then(function(tests) {
    if (!tests) {
      return res.status(404).send({
        message: 'No tests found'
      });
    } else {
      res.json(tests);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};


/**
 * Get tests by company
 */
exports.getTestsByCompany = function(req, res) {
  Test.findAll({
    where: {
      companyId: req.params.companyId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.speaking_test_result, db.candidate
    ]
  }).then( function(tests) {
    res.json(tests || null);
  });
};


/**
 * Get tests by client
 */
exports.getTestsByClient = function(req, res) {
  Test.findAll({
    where: {
      clientId: req.params.clientId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.speaking_test_result, db.candidate
    ]
  }).then( function(tests) {
    res.json(tests || null);
  });
};


/**
 * Get tests by trainer
 */
exports.getTestsByTrainer = function(req, res) {
  Test.findAll({
    where: {
      trainerId: req.params.trainerId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.speaking_test_result, db.candidate
    ]
  }).then( function(tests) {
    res.json(tests || null);
  });
};


/**
 * Get tests from specified date by language and trainer
 */
exports.getTestsByDateTrainer = function(req, res) {
  Test.findAll({
    where: {
      trainerId: req.query.trainerId,
      startsAt: {
        $between: [req.query.dateFrom, req.query.dateTo]
      }
    }
  }).then( function(exisingTests) {
    res.json(exisingTests || null);
  });
};


/**
 * Speaking tests middleware
 */
exports.speaking_testByID = function(req, res, next, id) {
  
  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Speaking test is invalid'
    });
  }

  Test.find({
    where: {
      id: id
    },
    include: [
      {model: db.trainer, include: [db.user]}, 
      {model: db.client, include: [db.user, db.company]}, 
      db.language, db.speaking_test_result, db.candidate
    ]
  }).then(function(test) {
    if (!test) {
      return res.status(404).send({
        message: 'No speaking test with that identifier has been found'
      });
    } else {
      req.test = test;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};
