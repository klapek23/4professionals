'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash'),
  fs = require('fs'),
  async = require('async'),
  path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')).models,
  multer = require('multer'),
  clone = require('clone');
  
  
  
  
// Add multipart handling middleware
exports.speakingTestsResultMulterStorage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './public/uploads/speaking_tests/results');
  },
  filename: function(req, file, cb) {
    var getFileExt = function(fileName) {
      var fileExt = fileName.split(".");
      if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
        return "";
      }
      return fileExt.pop();
    };
    
    cb(null, file.originalname + '_' + Date.now() + '.' + getFileExt(file.originalname));
  }
});
  
  
/**
 * Update speaking test result
 */
exports.changeTestResult = function(req, res) {
  db.speaking_test.findOne({
    where: {
      id: req.params.testId
    },
    include: [db.speaking_test_result]
  }).then(function(test) {
    if (test) {
      if (!req.file) {
        return res.status(400).send({
          message: 'Error occurred while uploading file'
        });
      } else {
        var oldFile = (test.speaking_test_result ? test.speaking_test_result : undefined);
        
        if(oldFile) {
          var newFile = clone(oldFile);
          
          newFile.result_file = req.file.filename;
          newFile.mime = req.file.mimetype;
          newFile.destination = req.file.destination;
          newFile.size = req.file.size;
          newFile.notes = '';
          
          return newFile.save().then(function(newFile) {
            try {
              var stats = fs.lstatSync('./public/uploads/speaking_tests/results/' + oldFile.result_file);
              
              if (stats.isFile()) {
                fs.unlinkSync('./public/uploads/speaking_tests/results/' + oldFile.result_file);
              }
            } catch (e) {
              console.log('Unable to delete the old file', e);
            }
            
            res.json(newFile);
          });
          
        } else {
          return db.speaking_test_result.create({
            result_file: req.file.filename,
            mime: req.file.mimetype,
            destination: req.file.destination,
            size: req.file.size,
            notes: '',
            speakingTestId: req.params.testId
          }).then(function(result) {
            if (!result) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(result)
              });
            } else {
              return test.updateAttributes({
                status: 'done'
              }).then(function(test) {
                res.json(result);
              });
            }
          }, function(err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          });
        }
      }

    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};


/**
 * Download speaking test result
 */
exports.downloadResult = function(req, res) {
  db.speaking_test_result.findOne({
    where: {
      speakingTestId: req.params.testId
    }
  }).then(function(testResult) {
    var file = './public/uploads/speaking_tests/results/' + testResult.result_file;
    res.download(file); // Set disposition and send it.
  }, function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};
