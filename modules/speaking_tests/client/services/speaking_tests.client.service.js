'use strict';

// Speaking tests service used for communicating with the users REST endpoint
angular.module('speaking_tests').factory('Speaking_test', ['$resource',
  function($resource) {
    return $resource('api/speaking_tests/:testId', {
      testId: '@id'
    }, {
      update: {
        method: 'PUT'
      },
      getRunningTests: {
        method: 'GET',
        url: 'api/speaking_tests/running-tests/get',
        isArray: true
      },
      getTestsByCompany: {
        method: 'GET',
        url: 'api/speaking_tests/get/by-company/:companyId',
        params: {
          clientId: '@companyId'
        },
        isArray: true
      },
      getTestsByClient: {
        method: 'GET',
        url: 'api/speaking_tests/get/by-client/:clientId',
        params: {
          clientId: '@clientId'
        },
        isArray: true
      },
      getTestsByTrainer: {
        method: 'GET',
        url: 'api/speaking_tests/get/by-trainer/:trainerId',
        params: {
          trainerId: 'trainerId'
        },
        isArray: true
      },
      getTestsByDateTrainer: {
        method: 'GET',
        url: 'api/speaking_tests/get/by-date-trainer',
        params: {
          dateFrom: '@dateFrom',
          dateTo: '@dateTo',
          trainerId: '@trainerId'
        },
        isArray: true
      }
    });
  }
]);