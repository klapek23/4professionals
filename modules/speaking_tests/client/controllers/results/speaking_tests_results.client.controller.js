'use strict';

// Speaking tests controller
angular.module('speaking_tests').controller('Speaking_testResultController', ['$scope', '$stateParams', '$location', 'Authentication',  'Speaking_test', 'Trainers', '$state', '$filter', '$timeout', 'notify', '$window', 'FileUploader', function($scope, $stateParams, $location, Authentication, Speaking_test, Trainers, $state, $filter, $timeout, notify, $window, FileUploader) {
    $scope.authentication = Authentication;
    $scope.showSpinner = false;
    
    // Create file uploader instance
    $scope.uploader = new FileUploader({
      url: 'api/speaking_tests/results/test/' + $stateParams.testId,
      alias: 'speakingTestResult'
    });
    
    // Set file uploader file filter
    $scope.uploader.filters.push({
      name: 'fileFilter',
      fn: function(item, options) {
        var aName = item.name.split('.');
        if(['doc', 'docx', 'pdf', 'xls', 'xlsx', 'odt', 'txt'].indexOf(aName[aName.length - 1]) !== -1) {
          return true;
        } else {
          return false;
        }
      }
    });
    
    // Called after the user selected a new picture file
    $scope.uploader.onAfterAddingFile = function(fileItem) {
      $scope.fileURL = fileItem.file.name;
    };
    
    // Called after the user has successfully uploaded a new picture
    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
      // Show success message
      $scope.success = true;

      // Populate user object
      $scope.user = Authentication.user = response;

      // Clear upload buttons
      $scope.cancelUpload();
    };

    // Called after the user has failed to uploaded a new picture
    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
      // Clear upload buttons
      $scope.cancelUpload();

      // Show error message
      $scope.error = response.message;
    };

    // Change user profile picture
    $scope.uploadTestResult = function() {
      $scope.showSpinner = true;

      // Clear messages
      $scope.success = $scope.error = null;

      // Start upload
      $scope.uploader.uploadAll();
    };

    // Cancel the upload process
    $scope.cancelUpload = function() {
      $scope.showSpinner = false;
      $scope.uploader.clearQueue();
    };
  }
]);