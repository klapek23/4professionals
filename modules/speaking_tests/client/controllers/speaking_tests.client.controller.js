'use strict';

// Speaking tests controller
angular.module('speaking_tests').controller('Speaking_testController', ['$scope', '$stateParams', '$location', 'Authentication', 'languagesResolve', 'Speaking_test', 'Trainers', 'TrainerAvailability', 'TrainerAvailabilities', '$state', '$filter', 'notify', 'moment','Clients', '$window', 'FileUploader', 'Socket', function($scope, $stateParams, $location, Authentication, languagesResolve,  Speaking_test, Trainers,  TrainerAvailability, TrainerAvailabilities, $state, $filter, notify, moment, Clients, $window, FileUploader, Socket) {
    $scope.authentication = Authentication;
    $scope.showSpinner = false;
    
    $scope.languages = languagesResolve;
    $scope.levels = ['A1', 'A1 25%', 'A1 50%', 'A1 75%', 'A2', 'A2 25%', 'A2 50%', 'A2 75%', 'B1', 'B1 25%', 'B1 50%', 'B1 75%', 'B2', 'B2 25%', 'B2 50%', 'B2 75%', 'C1', 'C1 25%', 'C1 50%', 'C1 75%', 'C2', 'N/A'];
    $scope.statuses = ['pending', 'done', 'N/A'];
    
    //check socket conneciont
    if (!Socket.socket) {
      Socket.connect();
    }
    
    Socket.on('trainersAvailabilityChange', function(message) {
      if(($scope.test.trainer && $scope.test.trainer.id === message.data.test.trainerId) && ($scope.authentication.user.id !== message.data.user.id)) {
        $scope.getTrainerAvailabilableDays();
        notify({ classes: 'alert-info', message: 'Availability of the selected trainer has changed, if You selected test time or date, please select it again' });
      }
    });
    
    if($state.current.name === 'speaking_tests.edit') {
      $scope.stateEdit = true;
    }
    
    if($scope.authentication.user.roles === 'admin') {
      Clients.query().$promise.then(function(clients) {
        $scope.clients = [];
        
        angular.forEach(clients, function(client, i) {
          if(client.hasOwnProperty('client_permission') && client.client_permission.speaking_tests === true) {
            client.nameWithCompany = client.user.displayName + ' - ' + client.company.name;
            $scope.clients.push(client);
          }
        });
      }, function(err) {
        console.log(err);
      });
    }
    
    $scope.availableTimes = [];
    $scope.trainerNotAvailable = [];
    $scope.test = {};

    moment.locale('pl', {
      week : {
        dow : 1 // Monday is the first day of the week
      }
    });
    
    $scope.datepickerOptions = {
      showWeeks: false,
      startingDay: 'Monday',
      opened: false,
      availableDays: function(date, mode) {
        if(mode === 'day') {
          //this may be error
          if($scope.test.availableDays.indexOf(moment(date).format('YYYY-MM-DD')) !== -1 && moment(date) >= moment()) {            
            return false;
          } else {
            return true;
          }
        }
      },
      toggleCalendar: function() {
        if($scope.stateEdit) {
          //edit
          if($scope.authentication.user.roles === 'admin' && !$scope.test.dateDisabled) {
            this.opened = !this.opened;
          }
        } else {
          //create
          if(!$scope.test.dateDisabled) {
            this.opened = !this.opened;
          }
        }
      }
    };
    
    $scope.getAvailableDaysByLanguage = function() {
      TrainerAvailabilities.getAvailableDaysByLanguage({languageId: $scope.test.language.id}).then(function(days) {
        $scope.test.availableDays = days;
      }, function(err) {
        console.log(err);
      });
    };
    
    $scope.getTrainersByLanguage = function() {
      Trainers.getByLanguage({languageId: $scope.test.language.id}).$promise.then(function(trainers) {
        angular.forEach(trainers, function(trainer, i) {
          trainer.nameWithEmail = trainer.user.displayName + ' - ' + trainer.user.email;
        });
        
        $scope.test.trainersByLanguage = trainers;
        
        $scope.test.date = undefined;
        $scope.test.availableTimes = undefined;
        $scope.test.trainer = undefined;
        $scope.test.dateDisabled = false;
        $scope.test.timeRange = undefined;
        $scope.trainerNotAvailable = [];
      });
    };
    
    
    $scope.getAvailableTrainersByDateAndLanguage = function() {
      Trainers.getByDateAndLanguage({languageId: $scope.test.language.id, date: moment($scope.test.date).format('YYYY-MM-DD')}).$promise.then(function(trainers) {
        angular.forEach(trainers, function(trainer, i) {
          trainer.nameWithEmail = trainer.user.displayName + ' - ' + trainer.user.email;
        });
        
        $scope.test.trainersByDateAndLanguage = trainers;
        
        $scope.test.availableTimes = undefined;
        $scope.test.trainer = undefined;
        $scope.test.trainerDisabled = false;
        $scope.test.timeRange = undefined;
        $scope.trainerNotAvailable = [];
      });
    };
    
    
    $scope.getTrainersByLanguageAndDate = function() {
      Trainers.getByLanguageAndDate({languageId: $scope.test.language.id, date: $scope.test.date}).$promise.then(function(trainers) {
        angular.forEach(trainers, function(trainer, i) {
          trainer.nameWithEmail = trainer.user.displayName + ' - ' + trainer.user.email;
        });
        
        $scope.test.trainersByLanguageAndDate = trainers;
        $scope.test.trainerDisabled = false;
        
        $scope.test.date = undefined;
        $scope.test.availableTimes = undefined;
        $scope.test.trainer = undefined;
        $scope.test.dateDisabled = false;
        $scope.test.timeRange = undefined;
        $scope.trainerNotAvailable = [];
      });
    };
    
    
    $scope.getTrainerAvailabilableDays = function() {
      TrainerAvailability.getAvailableDays({trainerId: $scope.test.trainer.id}).$promise.then(function(availability) {
        $scope.test.availableDays = availability;
        $scope.test.date = undefined;
        $scope.test.availableTimes = undefined;
        $scope.test.dateDisabled = false;
        $scope.test.timeRange = undefined;
        $scope.trainerNotAvailable = [];
      });
    };
    
    
    $scope.getTrainerAvailableTimeByDate = function() {
       TrainerAvailability.getAvailableTimeByDate({trainerId: $scope.test.trainer.id, dateFrom: moment($scope.test.date).format('YYYY-MM-DD HH:mm'), dateTo: moment($scope.test.date).add(1, 'day').format('YYYY-MM-DD HH:mm')}).$promise.then(function(data) {
         $scope.test.availableTimes = data;
         
         $scope.test.availableTimeRanges = [];
         angular.forEach(data, function(el,i) {
          $scope.test.availableTimeRanges.push(moment.range(el.startsAt, el.endsAt));
         });
         
         $scope.test.availableSlots = [];
         var quarter = moment.range(moment(), moment().add(15, 'm'));
         
         angular.forEach($scope.test.availableTimeRanges, function(el, i) {
           el.by(quarter, function(slot) {
            $scope.test.availableSlots.push(slot.format('HH:mm') + ' - ' + slot.add(15, 'm').format('HH:mm'));
           }, true);
         });
         
         Speaking_test.getTestsByDateTrainer({
           dateFrom: moment($scope.test.date).format('YYYY-MM-DD HH:mm'), 
           dateTo: moment($scope.test.date).add(1, 'day').format('YYYY-MM-DD HH:mm'), 
           trainerId: $scope.test.trainer.id
         }).$promise.then(function(existingTests) {
           $scope.trainerNotAvailable = [];
           
           angular.forEach(existingTests, function(el,i) {
             if(el.status !== 'canceled') $scope.trainerNotAvailable.push(moment(el.startsAt).format('HH:mm') + ' - ' + moment(el.endsAt).format('HH:mm'));
           });
         });
       });
    };
    
    
    $scope.prepareFormData = function() {
      $scope.test.trainerDisabled = true;
      $scope.test.dateDisabled = true;
      $scope.test.startsAt = moment().ceil(15, 'minutes');
      $scope.test.endsAt = moment().ceil(15, 'minutes').add(15, 'minutes');
      $scope.test.maxTime = moment($scope.endsAt).subtract(15, 'minutes');
      $scope.test.minTime = moment($scope.startsAt).add(15, 'minutes');
    };

    // Create new test
    $scope.create = function(isValid) {
      $scope.error = null;
      $scope.submitted = true;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'speaking_testForm');
        return false;
      }

      $scope.showSpinner = true;

      var aTime = $scope.test.timeRange.split('-');
      angular.forEach(aTime, function(el,i) {
        aTime[i] = el.trim();
      });
      
      // Create new Test object
      var test = new Speaking_test({
        startsAt: moment(moment(this.test.date).format('YYYY-MM-DD') + ' ' + aTime[0]).utc().toDate(),
        endsAt: moment(moment(this.test.date).format('YYYY-MM-DD') + ' ' + aTime[1]).utc().toDate(),
        notes: this.test.notes,
        candidateFullName: this.test.candidateFullName,
        candidatePhone: this.test.candidatePhone,
        //candidateEmail: this.test.candidateEmail,
        languageId: this.test.language.id,
        trainerId:  this.test.trainer.id,
        clientId:  ($scope.authentication.user.roles !== 'admin' ? $scope.authentication.user.client.id : this.test.client.id),
        companyId:  ($scope.authentication.user.roles !== 'admin' ? $scope.authentication.user.client.company.id : this.test.client.company.id)
      });

      // Redirect after save
      test.$save().then(function(response) {
        $scope.showSpinner = false;

        // Clear form fields
        $scope.test.startsAt = '';
        $scope.test.endsAt = '';
        $scope.test.notes = '';
        $scope.test.candidateFullName = '';
        $scope.test.candidatePhone = '';
        //$scope.test.candidateEmail = '';
        $scope.test.trainerId = '';
        $scope.test.clientId = '';
        
        Socket.emit('trainersAvailabilityChange', {
          test: response,
          user: $scope.authentication.user
        });
        
        $location.path('/speaking_tests');
        
        notify({ classes: 'alert-success', message: 'New speaking test has been added' });
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Test
    $scope.remove = function(test) {
      if (test) {

        test.$remove();
        if($location.$$path !== '/speaking_tests') {
          $location.path('speaking_tests');
        } else {
          $state.reload();
        }
        
        notify({ classes: 'alert-success', message: 'Speaking test has been removed' });
      } else {
        $scope.test.$remove(function() {
          if($location.$$path !== '/speaking_tests') {
            $location.path('speaking_tests');
          } else {
            $state.reload();
          }
          
          notify({ classes: 'alert-success', message: 'Speaking test has been removed' });
        });
      }
    };

    // Update existing Test
    $scope.update = function(isValid) {
      $scope.error = null;
      $scope.submitted = true;
      
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'speaking_testForm');
        return false;
      }

      $scope.showSpinner = true;
      
      var aTime = $scope.test.timeRange.split('-');
      angular.forEach(aTime, function(el,i) {
        aTime[i] = el.trim();
      });

      $scope.test.startsAt = moment(moment($scope.test.date).format('YYYY-MM-DD') + ' ' + aTime[0]).utc().toDate();
      $scope.test.endsAt = moment(moment($scope.test.date).format('YYYY-MM-DD') + ' ' + aTime[1]).utc().toDate();
      $scope.test.languageId = $scope.test.language.id;
      $scope.test.trainerId =  $scope.test.trainer.id;
      $scope.test.clientId = ($scope.authentication.user.roles === 'client' ? $scope.authentication.user.client.id : $scope.test.client.id);
      $scope.test.companyId = ($scope.authentication.user.roles === 'client' ? $scope.authentication.user.client.company.id : this.test.client.company.id);
      $scope.test.level = ($scope.test.level ? $scope.test.level : null);
      
      var test = $scope.test;

      test.$update(function() {
        if($scope.uploader && $scope.uploader.queue.length > 0) {
          $scope.uploader.onCompleteAll = function() {
            $scope.showSpinner = false;
            $location.path('/speaking_tests');
            notify({ classes: 'alert-success', message: 'Speaking test has been updated' });
          };
                   
          $scope.uploader.uploadAll();
        } else {
          $scope.showSpinner = false;
          $location.path('/speaking_tests');
          notify({ classes: 'alert-success', message: 'Speaking test has been updated' });
        }
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };

    // Find existing Test
    $scope.findOne = function() {
      Speaking_test.get({
        testId: $stateParams.testId
      }).$promise.then(function(test) {
        $scope.test = test;
        
        $scope.test.candidateFullName = test.candidate.name;
        $scope.test.candidatePhone = test.candidate.phone;
        //$scope.test.candidateEmail = test.candidate.email;
        $scope.test.date = moment(test.startsAt).format('YYYY-MM-DD');
        
        Trainers.getByDateAndLanguage({languageId: $scope.test.language.id, date: $scope.test.date}).$promise.then(function(trainers) {
          angular.forEach(trainers, function(trainer, i) {
            trainer.nameWithEmail = trainer.user.displayName + ' - ' + trainer.user.email;
          });
          
          $scope.test.trainersByDateAndLanguage = trainers;
          $scope.test.trainerDisabled = false;
        });
        
        $scope.test.date = moment($scope.test.startsAt).format('YYYY-MM-DD');
        $scope.test.timeRange = moment($scope.test.startsAt).format('HH:mm') + ' - ' + moment($scope.test.endsAt).format('HH:mm');
        $scope.test.oldTimeRange = moment($scope.test.startsAt).format('HH:mm') + ' - ' + moment($scope.test.endsAt).format('HH:mm');
        
        TrainerAvailability.getAvailableDays({trainerId: $scope.test.trainer.id}).$promise.then(function(availability) {
          $scope.test.availableDays = availability;
        });
        
        $scope.getTrainerAvailableTimeByDate();
        
        if(moment().add(3, 'hours') <= moment($scope.test.startsAt) && ($scope.authentication.user.roles === 'admin' || $scope.authentication.user.roles === 'client')) {
          $scope.statuses.push('canceled');
        }
      });
    };
    
    
    // Create file uploader instance
    $scope.createUploader = function() {
      $scope.uploader = new FileUploader({
        url: 'api/speaking_tests/results/test/' + $stateParams.testId,
        alias: 'speakingTestResult',
        queueLimit: 1
      });
    
      // Set file uploader file filter
      $scope.uploader.filters.push({
        name: 'fileFilter',
        fn: function(item, options) {
          var aName = item.name.split('.');
          if(['doc', 'docx', 'pdf', 'xls', 'xlsx', 'odt', 'txt'].indexOf(aName[aName.length - 1]) !== -1) {
            return true;
          } else {
            return false;
          }
        }
      });
      
      return $scope.uploader;
    };
  }
]);
