'use strict';

// Speaking tests controller
angular.module('speaking_tests').controller('ListSpeaking_testController', ['$scope', '$stateParams', '$location', 'Authentication', 'Speaking_test', '$state', '$filter', 'notify', 'FileUploader', function($scope, $stateParams, $location, Authentication, Speaking_test, $state, $filter, notify, FileUploader) {
    $scope.authentication = Authentication;
    $scope.sortBy = 'createdAt';
    $scope.sortReverse = true;
    
    if($scope.authentication.user.roles === 'client') {
      Speaking_test.getTestsByCompany({companyId: $scope.authentication.user.client.company.id}).$promise.then(function(tests) {
        $scope.tests = tests;
        
        $scope.buildPager();
      });
    } else if($scope.authentication.user.roles === 'trainer') {
       Speaking_test.getTestsByTrainer({trainerId: $scope.authentication.user.trainer.id}).$promise.then(function(tests) {
         $scope.tests = tests;
         
         $scope.buildPager();
       });
    } else {
      Speaking_test.query().$promise.then(function(tests) {
        $scope.tests = tests;
        $scope.all = true;
        
        $scope.buildPager();
      });
    }
    
    
    // Remove existing Test
    $scope.remove = function(test) {
      if (test) {
        test.$remove().then(function() {
          notify({ classes: 'alert-success', message: 'Speaking test has been removed' });
          $state.reload();
        });        
      }
    };
    
    $scope.buildPager = function() {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 100;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function() {
      $scope.filteredItems = $filter('filter')($scope.tests, {
        $: $scope.search
      });
      
      if($scope.sortBy) {
        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortBy, $scope.sortReverse);
      }
      
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.sortTable = function(sortBy) {
      if(sortBy === $scope.sortBy) {
        $scope.sortReverse = !$scope.sortReverse;
      } else {
        $scope.sortReverse = false;
      }
      
      $scope.sortBy = sortBy;
      
      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortBy, $scope.sortReverse);
      
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function() {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.getAllTests = function() {
      $scope.tests = Speaking_test.query(function() {
        $scope.all = true;
        $scope.buildPager();
      });
    };
    
    $scope.getFutureTests = function() {
      $scope.tests = Speaking_test.getRunningTests(function() {
        $scope.all = false;
        $scope.buildPager();
      });
    };
    
    
    //Result files functions
    
    //Toggle tooltip menu
    $scope.toggleTooltipMenu = function(test, $event) {
      var $button = angular.element($event.currentTarget);
      var $menu   = angular.element($button.children('.tooltip-menu'));
      
      angular.element('.tooltip-menu').removeClass('active');
      $menu.addClass('active');
      
      $event.stopPropagation();
    };
    
    //hide all tooltips menu on body click
    angular.element('body').on('click', function(event) {
      angular.element('.tooltip-menu').removeClass('active');
    });
    
    // Create file uploader instance
    $scope.createUploader = function(test) {
      var uploader = new FileUploader({
        url: 'api/speaking_tests/results/test/' + test.id,
        alias: 'speakingTestResult',
        autoUpload: true
      });
    
      // Set file uploader file filter
      uploader.filters.push({
        name: 'fileFilter',
        fn: function(item, options) {
          var aName = item.name.split('.');
          if(['doc', 'docx', 'pdf', 'xls', 'xlsx', 'odt', 'txt'].indexOf(aName[aName.length - 1]) !== -1) {
            return true;
          } else {
            return false;
          }
        }
      });
      
      uploader.onSuccessItem = function(item, response, status, headers) {
        notify({ classes: 'alert-success', message: 'Speaking test result has been added' });
        $state.reload();
      };
      
      uploader.onErrorItem = function(item, response, status, headers) {
        notify({ classes: 'alert-danger', message: 'Speaking test result uploading error' });
      };
      
      return uploader;
    };
  }
]);