'use strict';

// Setting up route
angular.module('speaking_tests').config(['$stateProvider',
  function($stateProvider) {
    
    $stateProvider
      .state('speaking_tests', {
        url: '/speaking_tests',
        abstract: true,
        template: '<ui-view class="">',
        data: {
          roles: ['admin', 'trainer', 'client']
        }
      })
      .state('speaking_tests.list', {
        url: '',
        templateUrl: 'modules/speaking_tests/client/views/list-speaking_tests.client.view.html',
        controller: 'ListSpeaking_testController',
        data: {
          roles: ['admin', 'trainer', 'client']
        }
      })
      .state('speaking_tests.create', {
        url: '/create',
        templateUrl: 'modules/speaking_tests/client/views/create-speaking_tests.client.view.html',
        controller: 'Speaking_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }]
        },
        data: {
          roles: ['admin', 'client']
        }
      })
      .state('speaking_tests.edit', {
        url: '/:testId/edit',
        templateUrl: 'modules/speaking_tests/client/views/edit-speaking_tests.client.view.html',
        controller: 'Speaking_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }]
        },
        data: {
          roles: ['admin', 'client']
        }
      })
      .state('speaking_tests.view', {
        url: '/:testId',
        templateUrl: 'modules/speaking_tests/client/views/view-speaking_tests.client.view.html',
        controller: 'Speaking_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query();
          }]
        },
        data: {
          roles: ['admin', 'trainer', 'client']
        }
      })
      .state('speaking_tests.finish', {
        url: '/:testId/finish',
        templateUrl: 'modules/speaking_tests/client/views/finish-speaking_tests.client.view.html',
        controller: 'Speaking_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query();
          }]
        },
        data: {
          roles: ['admin', 'trainer']
        }
      })
      .state('speaking_tests_results', {
        url: '/speaking_tests_results',
        abstract: true,
        template: '<ui-view class="">'
      })
      .state('speaking_tests.create_result', {
        url: '/:testId/result/:resultId',
        templateUrl: 'modules/speaking_tests/client/views/results/create-speaking_tests-result.client.view.html',
        controller: 'Speaking_testResultController',
        data: {
          roles: ['admin', 'trainer']
        }
      });
  }
]);