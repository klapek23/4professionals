'use strict';

// Configuring the Articles module
angular.module('speaking_tests').run(['Menus',
  function(Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Speaking tests',
      state: 'speaking_tests',
      type: 'dropdown',
      roles: ['admin', 'trainer', 'client'],
      icon:  '<i class="fa fa-headphones"></i>',
      position: 1
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'speaking_tests', {
      title: 'Test list',
      state: 'speaking_tests.list',
      roles: ['admin', 'trainer', 'client']
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'speaking_tests', {
      title: 'Create a test',
      state: 'speaking_tests.create',
      roles: ['admin', 'client']
    });
  }
]);