'use strict';

angular.module('speaking_tests').directive('isDateSet', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
              if(!scope.date) {
                ctrl.$setValidity('isDateSet', false);
                return viewValue;
              } else {
                ctrl.$setValidity('isDateSet', true);
              }
                
            });
        }
    };
}]);