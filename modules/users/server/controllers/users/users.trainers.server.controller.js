'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')).models,
  Trainer = db.trainer,
  TrainerAvailability = db.trainer_availability,
  TrainersLanguages = db.trainers_languages,
  Language = db.language,
  moment = require('moment');

/**
 * Get all trainers
 */
exports.getTrainers = function(req, res, next) {
  Trainer.findAll({
    include: [db.user]
  }).then( function(trainers) {
    res.json(trainers || null);
  });
};


/**
 * Get all trainers by language
 */
exports.getTrainersByLanguage = function(req, res, next) {
  Language.findById(req.query.languageId).then(function(language) {
    if(language) {
      return language.getTrainers({include: [db.trainer_availability, db.user]}).then(function(trainers) {
        res.json(trainers || null);
      });
    } else {
      res.status(400).send({
        message: 'Language not found'
      });
    }
  });
};


/**
 * Get trainers by date and language
 */
exports.getTrainersByDateAndLanguage = function(req, res, next) {
  Language.findById(req.query.languageId).then(function(language) {
    if(language) {
      return language.getTrainers({include: [db.trainer_availability, db.user]}).then(function(trainers) {
        var availableTrainers = [];
          
          trainers.forEach(function(trainer,i) {
            trainer.trainer_availabilities.forEach(function(el,i) {
              if(moment(el.startsAt).isSame(el.endsAt, 'day')) {
                if(moment(el.startsAt).isSame(moment(req.query.date), 'day')) {
                  availableTrainers.push(trainer);
                }
              }
            });
          });

          availableTrainers = availableTrainers.reverse().filter(function (e, i, arr) {
              return arr.indexOf(e, i+1) === -1;
          }).reverse();
        
          res.json(availableTrainers || null);
      });
    } else {
      res.status(400).send({
        message: 'Language not found'
      });
    }
  });
};


/**
 * Get all trainers by language
 */
exports.getTrainersByDate = function(req, res, next) {
  Trainer.findAll({
    include: [db.user]
  }).then( function(trainers) {
    res.json(trainers || null);
  });
};



/**
 * Get all trainers availabilities
 */
exports.getAllTrainersAvailability = function(req, res, next) {
  TrainerAvailability.findAll({
    include: [db.trainer]
  }).then( function(trainersAvailabiity) {
    res.json(trainersAvailabiity || null);
  });
};


/**
 * Get available days by language
 */
exports.getAvailabilityDaysByLanguage = function(req, res, next) {
  Language.findById(req.query.languageId).then(function(language) {
    if(language) {
      return language.getTrainers({include: [db.trainer_availability]}).then(function(trainers) {
        var availableDays = [];
        trainers.forEach(function(trainer,i) {
          trainer.trainer_availabilities.forEach(function(el,i) {
            if(moment(el.startsAt).isSame(el.endsAt, 'day')) {
              availableDays.push(moment(el.startsAt).format('YYYY-MM-DD'));
            }
          });
        });
        
        availableDays = availableDays.reverse().filter(function (e, i, arr) {
            return arr.indexOf(e, i+1) === -1;
        }).reverse();

        res.json(availableDays || null);
      });
    } else {
      res.status(400).send({
        message: 'Language not found'
      });
    }
  });
};


/**
 * Get all available days
 */
exports.getTrainersAvailabilityDays = function(req, res, next) {
  TrainerAvailability.findAll().then( function(trainersAvailabiity) {
    var availableDays = [];
    trainersAvailabiity.forEach(function(el,i) {
      if(moment(el.startsAt).isSame(el.endsAt, 'day')) {
        availableDays.push(moment(el.startsAt).format('YYYY-MM-DD'));
      }
    });
    
    
    availableDays = availableDays.reverse().filter(function (e, i, arr) {
        return arr.indexOf(e, i+1) === -1;
    }).reverse();
    
    res.json(availableDays || null);
  });
};


/**
 * Get specified trainer available days
 */
exports.getTrainerAvailabilityDays = function(req, res, next) {
  TrainerAvailability.findAll({
    where: {
      trainerId: req.params.trainerId
    }
  }).then( function(trainersAvailabiity) {
    var availableDays = [];
    trainersAvailabiity.forEach(function(el,i) {
      if(moment(el.startsAt).isSame(el.endsAt, 'day')) {
        availableDays.push(moment(el.startsAt).format('YYYY-MM-DD'));
      }
    });
    
    
    availableDays = availableDays.reverse().filter(function (e, i, arr) {
        return arr.indexOf(e, i+1) === -1;
    }).reverse();
    
    res.json(availableDays || null);
  });
};


/**
 * Get avaiability time by day
 */
exports.getTrainersAvailabilityTimeByDate = function(req, res, next) {
  TrainerAvailability.findAll({
    where: {
      startsAt: {
        $between: [req.query.dateFrom, req.query.dateTo]
      }
    }
  }).then( function(availableTime) {
    res.json(availableTime || null);
  });
};


/**
 * Get trainer avaiability time by day
 */
exports.getTrainerAvailabilityTimeByDate = function(req, res, next) {
  TrainerAvailability.findAll({
    where: {
      trainerId: req.params.trainerId,
      startsAt: {
        $between: [req.query.dateFrom, req.query.dateTo]
      }
    }
  }).then( function(availableTime) {
    res.json(availableTime || null);
  });
};


/**
 * Get available trainers by day
 */
exports.getAvailableTrainersByDay = function(req, res, next) {
  
};