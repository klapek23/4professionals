'use strict';

// User Authentication for MySQL, MariaDB, SQLite and MSSQL database dialects 

/** 
* File Path: /modules/users/server/controllers/users/user.authentication.server.controller.js
* File version: 0.1
*/

/**
 * Module dependencies.
 */
var path = require('path'),
  fs = require('fs'),
  request = require('request'),
  async = require('async'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  passport = require('passport'),
  db = require(path.resolve('./config/lib/sequelize')).models,
  User = db.user;

// URLs for which user can't be redirected on signin
var noReturnUrls = [
  '/authentication/signin',
  '/writing_tests/fill/*'
];

/**
 * Signup
 */
//exports.signup = function(req, res) {
//  // For security measurement we remove the roles from the req.body object
//  delete req.body.roles;
//
//  var message = null;
//
//  var user = User.build(req.body);
//
//  user.provider = 'local';
//  user.salt = user.makeSalt();
//  user.hashedPassword = user.encryptPassword(req.body.password, user.salt);
//  user.displayName = user.firstName + ' ' + user.lastName;
//
//  //MUST DELETE THIS WHEN PRODUCTION
//  if (req.body.is_admin === true) {
//    user.roles = ["admin"];
//  } else {
//    user.roles = [req.body.roles];
//  }
//
//  user.save().then(function() {
//    req.login(user, function(err) {
//      if (err)
//        res.status(400).send({
//          message: errorHandler.getErrorMessage(err)
//        });
//      res.json(user);
//    });
//  }).catch(function(err) {
//    res.status(400).send({
//      message: errorHandler.getErrorMessage(err)
//    });
//  });
//};

/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {
  
  passport.authenticate('local', function(err, user, info) {

    if (err || !user) {
      res.status(400).send({
        message: err
      });
    } else {
      // Remove sensitive data before login
      user.password = undefined;
      user.salt = undefined;

      req.login(user, function(err) {
        if (err) {
          res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(user);
        }
      });
    }
  })(req, res, next);
};

/**
 * Signout
 */
exports.signout = function(req, res) {
  req.logout();
  res.redirect('/');
};


var getFileExt = function(fileName) {
  var fileExt = fileName.split(".");
  if (fileExt.length === 1 || (fileExt[0] === "" && fileExt.length === 2)) {
    return "";
  }
  return fileExt.pop();
};
