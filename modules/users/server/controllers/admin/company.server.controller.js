'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  config = require(path.resolve('./config/config')),
  User = db.user,
  Client = db.client,
  Company = db.company;


/**
 * Update a company
 */
exports.getCompany = function(req, res) {
  Company.find({
    where: {
      id: req.params.companyId
    }
  }).then(function(company) {
    res.json(company);
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};



/**
 * Update a company
 */
exports.update = function(req, res) {
  Company.find({
    where: {
      id: req.params.companyId
    }
  }).then(function(company) {
    if (company) {

      company.name = req.body.name;
      company.address = req.body.address;
      company.email = req.body.email;
      company.updatedAt = Date.now();

      return company.save().then(function(company) {
        res.json(company);
      }).catch(function(err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });
    } else {
      return res.status(400).send({
        message: 'Could not find company'
      });
    }
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * Create a company
 */
exports.create = function(req, res) {
  
  var message = null;

  var company = Company.build(req.body);
  
  company.name = req.body.name;
  company.address = req.body.address;
  company.email = req.body.email;
  company.updatedAt = Date.now();

  company.save().then(function() {
    res.json(company);
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Delete a company
 */
exports.delete = function(req, res) {

    return Company.find({
      where: {
        id: req.params.companyId
      }
    }).then(function(company) {
      if (company) {
        return company.destroy().then(function() {
          return res.json(company);
        }).catch(function(err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });
      }
    });
    
};

/**
 * List of companies
 */
exports.list = function(req, res) {
  Company.findAll({
    order: [
      ['createdAt', 'DESC']
    ],
    include: [db.client]
  }).then(function(companies) {
    if (!companies) {
      return res.status(400).send({
        message: 'Unable to get list of companies'
      });
    } else {
      res.json(companies);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};