'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  config = require(path.resolve('./config/config')),
  User = db.user,
  Client = db.client,
  Trainer = db.trainer,
  generator = require('generate-password'),
  nodemailer = require('nodemailer'),
  smtpTransport = nodemailer.createTransport(config.mailer.options);


/**
 * Show the current user
 */
exports.read = function(req, res) {
  res.json(req.model);
};

/**
 * Update a User
 */
exports.update = function(req, res) {
  User.find({
    where: {
      id: req.model.id
    }
  }).then(function(user) {
    if (user) {

      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.displayName = req.body.firstName + ' ' + req.body.lastName;
      user.username = req.body.username;
      user.email = req.body.email;
      user.city = req.body.city;
      user.phone = req.body.phone;
      user.roles = req.body.roles;
      user.updatedAt = Date.now();
      
      if(user.roles === 'trainer') {
        user.languages = req.body.trainer.languages;
      } else if(user.roles === 'client') {
        user.company = req.body.client.company;
      }

      return user.save().then(function(user) {
        if(user.roles === 'trainer' && user.languages) {
          return Trainer.find({
            where: {
              userId: user.id
            }
          }).then(function(trainer) {
            if(trainer) {
              user.languages.forEach(function(el, i) {
                user.languages[i] = db.language.build(el);
              });
              trainer.setLanguages(user.languages);
              
              return trainer.save().then(function() {
                res.json(user);
              }).catch(function(err) {
                res.status(400).send({
                  message: errorHandler.getErrorMessage(err)
                });
              });
            } else {
              return res.status(400).send({
                message: 'Could not find trainer'
              });
            }
          });
        } else if (user.roles === 'client') {
          return Client.find({
            where: {
              userId: user.id
            }
          }).then(function(client) {
            if(client) {
              return client.save().then(function(client) {
                var company = db.company.build(user.company);
                
                return client.setCompany(company).then(function(company) {
                  res.json(user);
                }, function(err) {
                  res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                  });
                });
              }).catch(function(err) {
                res.status(400).send({
                  message: errorHandler.getErrorMessage(err)
                });
              });
            } else {
              return res.status(400).send({
                message: 'Could not find client'
              });
            }
          });
        } else {
          res.json(user);
        }
      }).catch(function(err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Could not find user'
      });
    }
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });

};

/**
 * Create a user
 */
exports.create = function(req, res) {
  
  var message = null;

  var user = User.build(req.body);
  
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;
  user.displayName = req.body.firstName + ' ' + req.body.lastName;
  user.username = req.body.username;
  user.email = req.body.email;
  user.city = req.body.city;
  user.phone = req.body.phone;
  user.roles = req.body.roles;
  user.is_active = 1;
  user.provider = 'local';
  user.salt = user.makeSalt();
  user.password = generator.generate({
                            length: 10,
                            numbers: true,
                            symbols: true
                          });
  user.hashedPassword = user.encryptPassword(user.password, user.salt);
  user.updatedAt = Date.now();

  user.save().then(function() {
      if(user.roles === 'client') {
        user.company = req.body.client.company;
        
        return Client.create({
          userId: user.id
        }).then(function(client) {
          var company = db.company.build(user.company);
          
          return client.setCompany(company).then(function(company) {
            
            return db.client_permissions.create({
              clientId: client.id,
              speaking_tests: true
            });
            
          });
        }, function(err) {
          return res.status(400).send({
            message: 'Create client error'
          });
        });
        
      } else if (user.roles === 'trainer') {
        user.languages = req.body.trainer.languages;
        
        return Trainer.create({
          userId: user.id
        }).then(function(trainer) {
          user.languages.forEach(function(el, i) {
            user.languages[i] = db.language.build(el);
          });
          return trainer.setLanguages(user.languages);
        }, function(err) {
          return res.status(400).send({
            message: 'Failure sending email'
          });
        });
      }
  }).then(function() {
    var emailHTML;
    
    res.render(path.resolve('modules/users/server/templates/create-user-confirmation-email'), {
      name: user.displayName,
      appName: config.app.title,
      username: user.username,
      pass: user.password,
      role: user.roles,
      url: config.app.mainUrl,
      supportEmail: config.app.supportEmail
    }, function(err, emailHTML) {
      if(err) {
        res.send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        
        //mail to new user
        var mailOptions = {
          to: user.email,
          from: config.mailer.from,
          subject: 'Account created',
          html: emailHTML
        };
      smtpTransport.sendMail(mailOptions, function(err) {
        if (!err) {
          var adminEmailHTML;
          
          //mail to administrator
          res.render(path.resolve('modules/users/server/templates/create-user-admin-confirmation-email'), {
            admin: config.admin.name,
            name: user.displayName,
            appName: config.app.title,
            username: user.username,
            pass: user.password,
            role: user.roles,
            url: config.app.mainUrl
          }, function(err, adminEmailHTML) {
            user.password = undefined;
            
            if(err) {
              res.send({
                message: errorHandler.getErrorMessage(err)
              });
            } else {
              var adminMailOptions = {
                to: config.admin.email,
                from: config.mailer.from,
                subject: 'New user account created',
                html: adminEmailHTML
              };
              smtpTransport.sendMail(adminMailOptions, function(err) {
                if (!err) {
                  res.send({
                    message: 'An account has been created.'
                  });
                } else {
                  return res.status(400).send({
                    message: 'Failure sending email'
                  });
                }
              });
            }
          });
          
        } else {
          return res.status(400).send({
            message: 'Failure sending email'
          });
        }
      });
      }
    });
    
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Delete a user
 */
exports.delete = function(req, res) {

  if (req.user.id === req.model.id) {
    return res.status(400).send({
      message: 'You cannot delete yourself!'
    });
  } else {
    return User.find({
      where: {
        id: req.model.id
      }
    }).then(function(user) {
      if (user) {
        return user.destroy().then(function() {
          return res.json(user);
        }).catch(function(err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });
      }
    });
  }
};

/**
 * List of Users
 */
exports.list = function(req, res) {
  User.findAll({
    order: [
      ['createdAt', 'DESC']
    ],
    include: [
      {model: db.trainer, include: [db.language]},
      {model: db.client, include: [db.company]}
    ]
  }).then(function(users) {
    if (!users) {
      return res.status(400).send({
        message: 'Unable to get list of users'
      });
    } else {
      res.json(users);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};


exports.getClients = function(req,res) {
  Client.findAll({
    order: [
      ['createdAt', 'DESC']
    ],
    include: [db.user, db.company, db.client_permissions]
  }).then(function(clients) {
    if (!clients) {
      return res.status(400).send({
        message: 'Unable to get list of clients'
      });
    } else {
      res.json(clients);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};


/**
 * User middleware
 */
exports.userByID = function(req, res, next, id) {
  if (!id) {
    return res.status(400).send({
      message: 'User is invalid'
    });
  }

  User.findOne({
    where: {
      id: id
    },
    include: [ 
      { model: db.client, include: [db.company] },
      { model: db.trainer, include: [db.trainer_availability, db.language] }
    ]
  }).then(function(user) {
    if (!user) {
      return next(new Error('Failed to load user ' + id));
    } else {
      
      var data = {};

      data.id = user.id;
      data.firstName = user.firstName;
      data.lastName = user.lastName;
      data.displayName = user.displayName;
      data.email = user.email;
      data.phone = user.phone;
      data.city = user.city;
      data.username = user.username;
      data.roles = user.roles;
      data.provider = user.provider;
      data.updatedAt = user.updatedAt;
      data.createdAt = user.createdAt;
      
      if(data.roles === 'trainer') {
        data.trainer = user.trainer;
      } else if(data.roles === 'client') {
        data.client = user.client;
      }

      req.model = data;
      next();
      return null;
    }
  }).catch(function(err) {
    return next(err);
  });

};