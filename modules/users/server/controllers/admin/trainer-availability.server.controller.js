'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  Availability = db.trainer_availability,
  config = require(path.resolve('./config/config'));


/**
  * Create new availability
  */
 exports.read = function(req, res) {
   Availability.find({
     where: {
       trainerId: req.params.trainerId,
       id: req.params.availabilityId
     }
   }).then(function(data) {
     res.json(data);
   }).catch(function(err) {
     res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
   });
 };
 

/**
  * Create new availability
  */
exports.create = function(req, res) {  
  var availability = Availability.build(req.body);

  availability.save().then(function(newAvailability) {
   res.json(newAvailability);
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
  * Update existing availability
  */
exports.update = function(req, res) {
   Availability.find({
     where: {
       trainerId: req.params.trainerId,
       id: req.params.availabilityId
     }
   }).then(function(availability) {
     if(!availability) {
       return res.status(400).send({
         message: 'Could not find user availability'
       });
     } else {
       availability.title = req.body.title;
       availability.startsAt = req.body.startsAt;
       availability.endsAt = req.body.endsAt;
       
       return availability.save().then(function(updatedAvailability) {
         res.json(updatedAvailability);
       }).catch(function(err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
       });
     }
   }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
 };
 
 
 /**
  * Delete new availability
  */
exports.delete = function(req, res) {
  Availability.destroy({
    where: {
      id: req.params.availabilityId,
      trainerId: req.params.trainerId
    }
  }).then(function(deletedAvailability) {
   res.json(deletedAvailability);
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * List of availabilities
 */
exports.list = function(req, res) {
  Availability.findAll({
    where: {
      id: req.params.trainerId
    },
    order: [
      ['createdAt', 'DESC']
    ],
    include: [db.trainer]
  }).then(function(availabilities) {
    if (!availabilities) {
      return res.status(400).send({
        message: 'Unable to get list of availabilities'
      });
    } else {
      res.json(availabilities);
    }
  }).catch(function(err) {
    res.jsonp(err);
  });
};