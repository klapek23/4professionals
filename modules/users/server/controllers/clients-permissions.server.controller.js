'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  config = require(path.resolve('./config/config')),
  User = db.user,
  Client = db.client,
  ClientPermissions = db.client_permissions;
  

/**
 * 
 * @param {obj} req
 * @param {obj} res
 * @returns {json}
 * 
 * Get all clients permisions
 */
exports.getPermissions = function(req, res) {
  ClientPermissions.findAll({
    include: [Client]
  }).then(function(permissions) {
    res.json(permissions);
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * 
 * @param {obj} req
 * @param {obj} res
 * @returns {json}
 * 
 * Get client permisions
 */
exports.getPermissionsByClientId = function(req, res) {
  ClientPermissions.findOne({
    where: {
      clientId: req.params.clientId
    },
    include: [Client]
  }).then(function(permissions) {
    res.json(permissions);
  }).catch(function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * 
 * @param {obj} req
 * @param {obj} res
 * @returns {json}
 * 
 * Set clients permisions
 */
exports.setPermissions = function(req, res) {
  var permissions = req.body.permissions;
  
  permissions.forEach(function(perm, i) {
    ClientPermissions.findOne({
      where: {
        clientId: perm.clientId,
      },
      include: [Client]
    }).then(function(permObj) {
      if(permObj) {
        return permObj.updateAttributes({
          speaking_tests: perm.speaking_tests,
          writing_tests: perm.writing_tests
        }).then(function(perm) {
          if(i+1 === permissions.length) {
            res.status(200).send({
              message: 'Permissions updated'
            });
          }
        }).catch(function(err) {
          res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });
        
      } else {
        return ClientPermissions.create(perm).then(function(perm) {
          if(i+1 === permissions.length) {
            res.status(200).send({
              message: 'Permissions updated'
            });
          }
        }).catch(function(err) {
          res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });
      }
    }).catch(function(err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
    
  });
};