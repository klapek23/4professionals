'use strict';

//Client model for MySQL

/** 
* File Path: /modules/users/server/models/client.server.model.js
* File Version: 0.2
*/

/**
 * Client Model
 */

module.exports = function(sequelize, DataTypes) {
  
  var Client = sequelize.define('client', {},
    {
      associate: function(models) {
        Client.belongsTo(models.user, {onDelete: 'cascade'});
        Client.belongsTo(models.company);
        Client.hasMany(models.speaking_test);
        Client.hasOne(models.client_permissions, {onDelete: 'cascade'});
      }
    }
  );
  
  return Client;
};
