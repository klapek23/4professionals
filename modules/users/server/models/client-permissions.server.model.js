'use strict';

//Client permissions model for MySQL

/**
 * Client permissions Model
 */

module.exports = function(sequelize, DataTypes) {
  
  var ClientPermissions = sequelize.define('client_permissions', {
    speaking_tests: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    writing_tests: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
    {
      associate: function(models) {
        ClientPermissions.belongsTo(models.client);
      }
    }
  );
  
  return ClientPermissions;
};
