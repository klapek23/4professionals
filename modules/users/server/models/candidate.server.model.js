"use strict";

module.exports = function(sequelize, DataTypes) {

  var Candidate = sequelize.define('candidate', {
    name: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: true
      }
    },
    phone: {
      type: DataTypes.TEXT
    },
    email: {
      type: DataTypes.TEXT,
      validate: {
        isEmail: true
      }
    }
  }, {
    associate: function(models) {
      Candidate.hasMany(models.speaking_test);
      Candidate.hasMany(models.writing_test);
    }
  });
  return Candidate;
};