'use strict';

//Client model for MySQL

/** 
* File Path: /modules/users/server/models/client.server.model.js
* File Version: 0.2
*/

/**
 * Client Model
 */

module.exports = function(sequelize, DataTypes) {
  
  var Company = sequelize.define('company', {
    name: {
      type: DataTypes.STRING,
      defaultValue: '',
      allowNull: false,
      unique: true
    },
    address: {
      type: DataTypes.STRING,
      defaultValue: ''
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    }
  },
  {
    associate: function(models) {
      Company.hasMany(models.client);
      Company.hasMany(models.speaking_test);
      Company.belongsToMany(models.writing_test_pattern,{through: 'writing_test_pattern_company'});
    }
  }
  );
  
  return Company;
};
