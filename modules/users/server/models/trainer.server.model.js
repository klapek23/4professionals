'use strict';

module.exports = function(sequelize, DataTypes) {
  var Trainer = sequelize.define('trainer', {},
    {
      associate: function(models) {
        Trainer.belongsTo(models.user, {onDelete: 'cascade'});
        Trainer.belongsToMany(models.language, {through: 'trainers_languages'});
        Trainer.hasMany(models.speaking_test);
        Trainer.hasMany(models.trainer_availability);
      }
    }
  );

  return Trainer;
};
