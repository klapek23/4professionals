"use strict";

var path = require('path'),
    db = require(path.resolve('./config/lib/sequelize')).models,
    moment = require('moment');
    
require('moment-range');

module.exports = function(sequelize, DataTypes) {

  var Trainer_availability = sequelize.define('trainer_availability', {
    title: {
      type: DataTypes.STRING
    },
    startsAt: {
      type: DataTypes.DATE,
//      validate: {
//        notEmpty: true,
//        isAfter: {
//          args: new Date(),
//          msg: 'Trainer start date has to be date later than current date'
//        }
//      }
    },
    endsAt: {
      type: DataTypes.DATE,
//      validate: {
//        notEmpty: true,
//        isAfter: {
//          args: [new Date()],
//          msg: 'Trainer end date has to be date later than current date'
//        },
//        isAfterStartDate: function() {
//          if (this.endsAt <= this.startsAt) {
//            throw new Error('Trainer end date has to be later than start date');
//          }
//        },
//      }
    },
    draggable: {
      type: DataTypes.BOOLEAN,
      default: true
    },
    resizable: {
      type: DataTypes.BOOLEAN,
      default: true
    },
    type: {
      type: DataTypes.STRING,
      default: 'success'
    }
  }, {
    associate: function(models) {
      Trainer_availability.belongsTo(models.trainer);
    }
  });
  return Trainer_availability; 
};