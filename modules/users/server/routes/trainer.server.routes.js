'use strict';

module.exports = function(app) {
  // User Routes
  var user = require('../controllers/user.server.controller'),
      trainerAvailability = require('../controllers/admin/trainer-availability.server.controller');
  
  app.route('/api/trainer/:trainerId/trainer-availability')
    .get(user.requiresLogin, trainerAvailability.list)
    .post(user.requiresLogin, trainerAvailability.create);
    
  app.route('/api/trainer/:trainerId/trainer-availability/:availabilityId')
    .get(user.requiresLogin, trainerAvailability.read)
    .put(user.requiresLogin, trainerAvailability.update)
    .delete(user.requiresLogin, trainerAvailability.delete);
    
  app.route('/api/trainer/:trainerId/trainer-available-days')
    .get(user.requiresLogin, user.getTrainerAvailabilityDays);
    
  app.route('/api/trainer/:trainerId/trainer-availability-by-date')
    .get(user.requiresLogin, user.getTrainerAvailabilityTimeByDate);
  
  app.route('/api/trainers')
    .get(user.requiresLogin, user.getTrainers);
    
  app.route('/api/trainers/by-language')
    .get(user.requiresLogin, user.getTrainersByLanguage);
    
  app.route('/api/trainers/by-date-and-language')
    .get(user.requiresLogin, user.getTrainersByDateAndLanguage);
    
  app.route('/api/trainers/by-date')
    .get(user.requiresLogin, user.getTrainersByDate);
    
  app.route('/api/trainers/trainers-availability')
    .get(user.requiresLogin, user.getAllTrainersAvailability);
    
  app.route('/api/trainers/available-days')
    .get(user.requiresLogin, user.getTrainersAvailabilityDays);
    
  app.route('/api/trainers/available-days/by-language')
    .get(user.requiresLogin, user.getAvailabilityDaysByLanguage);
    
  app.route('/api/trainers/trainer-availability/by-date')
    .get(user.requiresLogin, user.getTrainersAvailabilityTimeByDate);

  // Finish by binding the user middleware
  app.param('userId', user.userByID);
};
