'use strict';

module.exports = function(app) {
  // User Routes
  var user = require('../controllers/user.server.controller'),
      multer = require('multer'),
      clientsPermissions = require('../controllers/clients-permissions.server.controller');

	  // Setting up the users profile api
	  app.route('/api/user/me')
		.get(user.requiresLogin, user.me);
		
	  app.route('/api/user')
		.get(user.requiresLogin, user.getProfile)
		.put(user.requiresLogin, user.update);
		
	  app.route('/api/user/password')
		.post(user.requiresLogin, user.changePassword);
		
	  app.route('/api/user/picture')
		.post(user.requiresLogin, multer({storage: user.profilePictureStorage}).single('profilePicture'), user.changeProfilePicture);
        
          app.route('/api/admin/clients/permissions')
            .get(user.requiresLogin, clientsPermissions.getPermissions)
            .post(user.requiresLogin, clientsPermissions.setPermissions);
    
          app.route('/api/admin/clients/permissions/:clientId')
            .get(user.requiresLogin, clientsPermissions.getPermissionsByClientId);

	  // Finish by binding the user middleware
	  app.param('userId', user.userByID);
};
