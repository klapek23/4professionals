'use strict';

module.exports = function(app) {
  // User Routes
  var adminPolicy = require('../policies/admin.server.policy'),
      Company = require('../controllers/admin/company.server.controller');
  
  app.route('/api/company')
    .get(adminPolicy.isAllowed, Company.list)
    .post(adminPolicy.isAllowed, Company.create);
    
  app.route('/api/company/:companyId')
    .get(adminPolicy.isAllowed, Company.getCompany)
    .put(adminPolicy.isAllowed, Company.update)
    .delete(adminPolicy.isAllowed, Company.delete);
};
