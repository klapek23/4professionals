'use strict';

/**
 * Module dependencies.
 */
var adminPolicy = require('../policies/admin.server.policy'),
  admin = require('../controllers/admin/admin.server.controller'),
  trainerAvailability = require('../controllers/admin/trainer-availability.server.controller');

module.exports = function(app) {
  // User route registration first. Ref: #713
  //require('./user.server.routes.js')(app);

  app.route('/api/admin/user')
    .get(adminPolicy.isAllowed, admin.list);
    
    
  app.route('/api/admin/user/create')
    .put(adminPolicy.isAllowed, admin.create);


  app.route('/api/admin/user/:userId')
    .get(adminPolicy.isAllowed, admin.read)
    .put(adminPolicy.isAllowed, admin.update)
    .delete(adminPolicy.isAllowed, admin.delete);
    
  app.route('/api/admin/clients')
    .get(adminPolicy.isAllowed, admin.getClients);

  app.param('userId', admin.userByID);
};