'use strict';

/**
 * Module dependencies.
 */
var
  path = require('path'),
  passport = require('passport'),
  config = require(path.resolve('./config/config')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  User = db.user;

/**
 * Module init function.
 */
module.exports = function(app, db) {

  // Serialize sessions
  passport.serializeUser(function(user, done) {
    
    var userData = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      displayName: user.displayName,
      username: user.username,
      email: user.email,
      phone: user.phone,
      city: user.city,
      profileImageURL: user.profileImageURL,
      roles: user.roles,
      provider: user.provider,
      additionalProvidersData: user.additionalProvidersData
    };
    
    if(user.client) {
      userData.client = user.client.dataValues;
      userData.client.company = user.client.dataValues.company.dataValues;
    } else if(user.trainer) {
      userData.trainer = user.trainer.dataValues;
    }
    
    done(null, userData);
  });

  // Deserialize sessions
  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  // Initialize strategies
  config.utils.getGlobbedPaths(path.join(__dirname, './strategies/**/*.js')).forEach(function(strategy) {
    require(path.resolve(strategy))(config);
  });

  // Add passport's middleware
  app.use(passport.initialize());
  app.use(passport.session());
};