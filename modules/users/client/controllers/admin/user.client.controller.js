'use strict';

angular.module('user.admin').controller('UserController', ['$scope', '$state', '$http', 'Authentication', 'userResolve', 'languagesResolve', 'companiesResolve', 'notify', function($scope, $state, $http, Authentication, userResolve, languagesResolve, companiesResolve, notify) {

    $scope.authentication = Authentication;
    $scope.user = userResolve;
    $scope.languages = languagesResolve;
    $scope.companies = companiesResolve;
    
    $scope.remove = function() {

      if (confirm('Are you sure you want to delete this user?')) {

        var user = $scope.user;
        user.$remove({
          'userId': user.id
        }, function() {
          $state.go('admin.users');
          notify({ classes: 'alert-success', message: 'User has been removed' });
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      }
    };

    $scope.update = function(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');
        return false;
      }

      $scope.showSpinner = true;

      var user = $scope.user;

      user.$update({
        'userId': user.id
      }, function() {
        $scope.showSpinner = false;
        $state.go('admin.users');
        notify({ classes: 'alert-success', message: 'User has been updated' });
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };
    
    $scope.prepareFormData = function() {
      $scope.user.trainer = {};
      $scope.user.trainer.languageId = $scope.languages[0].id;
      $scope.user.roles = 'client';
    };
    
    $scope.create = function(isValid) {
      
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');
        return false;
      }

      $scope.showSpinner = true;

      $http.put('/api/admin/user/create', $scope.user).success(function(response) {
        $scope.showSpinner = false;
        $state.go('admin.users');
        notify({ classes: 'alert-success', message: 'User has been created' });
      }).error(function(response) {
        $scope.showSpinner = false;
        $scope.error = response.message;
      });
    };
  }
]);