'use strict';

angular.module('user.admin').controller('ClientsPermissionsController', ['$scope', '$state', '$http', 'Authentication', 'clientsResolve', 'ClientPermissions', 'notify', function($scope, $state, $http, Authentication, clientsResolve, ClientPermissions, notify) {

    $scope.authentication = Authentication;
    $scope.clients = clientsResolve;
    $scope.permissions = {};
    
    $scope.setClients = function() {
      angular.forEach($scope.clients, function(client, i) {
        $scope.permissions[client.id] = {};
        $scope.permissions[client.id].clientId = client.id;
      });
    };
    
    $scope.getPermissions = function() {
      ClientPermissions.getAll().then(function(response) {
        angular.forEach(response, function(perm, i) {
          $scope.permissions[perm.clientId] = perm;
        });
      });
    };
    
    $scope.updatePermissions = function(isValid) {
      var permissionsArray = [];
      
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'usersPermissionsForm');
        return false;
      }

      $scope.showSpinner = true;
      
      angular.forEach($scope.permissions, function(permission, i) {
        permission.speaking_tests = permission.speaking_tests ? permission.speaking_tests : false;
        permission.writing_tests = permission.writing_tests ? permission.writing_tests : false;
        
        permissionsArray.push(permission);
      });
      
      ClientPermissions.update(permissionsArray).then(function(response) {
        $scope.showSpinner = false;
        $state.reload();
        notify({ classes: 'alert-success', message: 'Permissions updated' });
      }, function(errorResponse) {
        $scope.showSpinner = false;
        notify({ classes: 'alert-danger', message: 'Permissions not updated, error: ' +  errorResponse.data.message});
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);