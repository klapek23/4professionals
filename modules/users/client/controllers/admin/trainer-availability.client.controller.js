'use strict';

angular.module('user.admin').controller('TrainerAvailabilityController', ['$scope', '$state', '$http', 'Authentication', 'userResolve', 'moment', 'TrainerAvailability', 'calendarConfig', 'notify', '$uibModal', function($scope, $state, $http, Authentication, userResolve, moment, TrainerAvailability, calendarConfig, notify, $uibModal) {
    
    $scope.authentication = Authentication;
    $scope.user = userResolve;
    
    // ---------- calendar config
    calendarConfig.displayEventEndTimes = true;
    $scope.isCellOpen = true;
    
    moment.locale('pl', {
      week : {
        dow : 1 // Monday is the first day of the week
      }
    });
    
    $scope.calendar = {
      view: 'month',
      date: new Date(),
      trainer: {
        availabilites: $scope.user.trainer.trainer_availabilities
      }
    };
    // ---------- calendar config
    
    // ---------- datepicker config
    $scope.datepickerOptions = {
        showWeeks: false,
        startingDay: 'Monday'
    };
    // ---------- datepicker config

    
    $scope.addNew = function() {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: '/modules/users/client/views/directives/trainer-availability-popup.html',
        controller: 'TrainerAvailabilityPopupController',
        size: 'lg',
        resolve: {
          item: null,
          trainer: function () {
            return $scope.user.trainer;
          }
        }
      });

      modalInstance.result.then(function (item) {
        item.type = 'success';
        item.draggable = true;
        item.resizable = true;
        item.range = moment.range(item.startsAt, item.endsAt);
        
        var error = false;
        
        if($scope.calendar.trainer.availabilites.length > 0) {
          $scope.calendar.trainer.availabilites.forEach(function(el, i) {
            var range = getRange(el);

            if((range.contains(item.startsAt) || range.overlaps(item.startsAt) || range.contains(item.endsAt) || range.overlaps(item.endsAt) || range.overlaps(item.range) || item.range.overlaps(range)) && el.id !== item.id) {
              error = true;
            }

            if(i + 1 === $scope.calendar.trainer.availabilites.length) {
              if(error) {
                notify({ classes: 'alert-danger', message: 'Error: Trainer already works at this time'});
                throw new Error("Trainer already works at this time");
              } else {
                if(item.repeat <= 1) {
                  var newAvailability = new TrainerAvailability(item);

                  newAvailability.$save({
                    userId: $scope.user.id
                  }).then(function(newAvailability) {
                    $scope.calendar.trainer.availabilites.push(newAvailability);
                    notify({ classes: 'alert-success', message: 'New trainer availability slot has been added' });
                  }).catch(function(err) {
                    notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
                  });
                } else {
                  var newAvailabilities = [];
                  var index = 0;

                  while(index < item.repeat) {
                    newAvailabilities[index] = new TrainerAvailability(item);
                    index++;
                  }

                  newAvailabilities.forEach(function(el, i) {
                    el.startsAt = moment(el.startsAt).add(i, 'weeks');
                    el.endsAt = moment(el.endsAt).add(i, 'weeks');

                    el.$save({
                      userId: $scope.user.id
                    }).then(function(newAvailability) {
                      $scope.calendar.trainer.availabilites.push(newAvailability);
                      notify({ classes: 'alert-success', message: 'New trainer availability slot has been added' });
                    }).catch(function(err) {
                      notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
                    });
                  });
                }
              }
            }
          });
        } else {
          if(item.repeat <= 1) {
            var newAvailability = new TrainerAvailability(item);

            newAvailability.$save({
              userId: $scope.user.id
            }).then(function(newAvailability) {
              $scope.calendar.trainer.availabilites.push(newAvailability);
              notify({ classes: 'alert-success', message: 'New trainer availability slot has been added' });
            }).catch(function(err) {
              notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
            });
          } else {
            var newAvailabilities = [];
            var index = 0;

            while(index < item.repeat) {
              newAvailabilities[index] = new TrainerAvailability(item);
              index++;
            }

            newAvailabilities.forEach(function(el, i) {
              el.startsAt = moment(el.startsAt).add(i, 'weeks');
              el.endsAt = moment(el.endsAt).add(i, 'weeks');

              el.$save({
                userId: $scope.user.id
              }).then(function(newAvailability) {
                $scope.calendar.trainer.availabilites.push(newAvailability);
                notify({ classes: 'alert-success', message: 'New trainer availability slot has been added' });
              }).catch(function(err) {
                notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
              });
            });
          }
        }
      });
    };
    
    
    $scope.edit = function(item) {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: '/modules/users/client/views/directives/trainer-availability-popup.html',
        controller: 'TrainerAvailabilityPopupController',
        size: 'lg',
        resolve: {
          item: item,
          trainer: function () {
            return $scope.user.trainer;
          }
        }
      });
      
      modalInstance.result.then(function (item) {
        var error = false;
        item.range = moment.range(item.startsAt, item.endsAt);

        $scope.calendar.trainer.availabilites.forEach(function(el, i) {
          var range = getRange(el);

          if((range.contains(item.startsAt) || range.overlaps(item.startsAt) || range.contains(item.endsAt) || range.overlaps(item.endsAt) || range.overlaps(item.range) || item.range.overlaps(range)) && el.id !== item.id) {
            error = true;
          }

          if(i + 1 === $scope.calendar.trainer.availabilites.length) {
            if(error) {
              notify({ classes: 'alert-danger', message: 'Error: Trainer already works at this time'});
              throw new Error("Trainer already works at this time");
            } else {            
              TrainerAvailability.update({userId: $scope.user.id, trainerId: item.trainerId, availabilityId: item.id }, item, 
                function(newData) {
                  item.startsAt = newData.startsAt; item.endsAt = newData.endsAt;
                  notify({ classes: 'alert-success', message: 'Trainer availability update successful' });
                }, function(err) {
                  notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
              });
            }
          }
        });
      });
    };
    
    
    $scope.updateFromCalendar = function(item, calendarNewEventStart, calendarNewEventEnd) {
      if(new Date(calendarNewEventStart).getTime() !== new Date(item.startsAt).getTime() || new Date(calendarNewEventEnd).getTime() !== new Date(item.endsAt).getTime()) {
        var error = false;
        var newItem = (JSON.parse(JSON.stringify(item)));
        newItem.range = moment.range(calendarNewEventStart, calendarNewEventEnd);
        newItem.startsAt = calendarNewEventStart;
        newItem.endsAt = calendarNewEventEnd;
        
        $scope.calendar.trainer.availabilites.forEach(function(el, i) {
            var range = getRange(el);
            
          if((range.contains(newItem.startsAt) || range.overlaps(newItem.startsAt) || range.contains(newItem.endsAt) || range.overlaps(newItem.endsAt) || range.overlaps(newItem.range) || newItem.range.overlaps(range)) && el.id !== newItem.id) {
            error = true;
          }
          
          if(i + 1 === $scope.calendar.trainer.availabilites.length) {
            if(error) {
              notify({ classes: 'alert-danger', message: 'Error: Trainer already works at this time'});
              //throw new Error("Trainer already works at this time");
            } else {
              TrainerAvailability.update({userId: $scope.user.id, trainerId: newItem.trainerId, availabilityId: newItem.id }, newItem, 
                function(newData) {
                  item.startsAt = moment(newData.startsAt).toDate(); 
                  item.endsAt = moment(newData.endsAt).toDate();
                  //getOccupiedRanges();
                  notify({ classes: 'alert-success', message: 'Trainer availability update successful' });
                }, function(err) {
                  notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
              });
            }
          }
        });
      }
    };
    
    
    $scope.delete = function(item, $index) {
      TrainerAvailability.get({userId: $scope.user.id, trainerId: item.trainerId, availabilityId: item.id}, function(availability) {
        availability.$delete({userId: $scope.user.id, trainerId: item.trainerId, availabilityId: availability.id }).then(function(deletedItem) {          
          $scope.calendar.trainer.availabilites = $scope.calendar.trainer.availabilites.filter(function (el) {
              return el.id !== item.id;
            }
          );
          //getOccupiedRanges();
          notify({ classes: 'alert-success', message: 'Trainer availability removed successful' });
        }).catch(function(err) {
          notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
        });
      }, function(err) {
        notify({ classes: 'alert-danger', message: 'Error: ' + err.data.message });
      });
    };
    
    
    function getRange(el) {
      var startDate = el.startsAt,
          endsDate = el.endsAt,
          dateRange = moment.range(startDate, endsDate);
      
      return dateRange;
    }
  }
]);