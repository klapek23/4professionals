'use strict';

angular.module('user.admin').controller('TrainerAvailabilityPopupController', ['$scope', '$uibModalInstance', 'Authentication', 'moment', 'TrainerAvailability', 'notify', 'item', 'trainer', function($scope, $uibModalInstance, Authentication, moment, TrainerAvailability, notify, item, trainer) {
    
    $scope.authentication = Authentication;
    $scope.calendarOpen = false;
    $scope.calendarMinDate = moment().add(1, 'day');
    
    if(item) {
      $scope.item = item;
      $scope.item.date = moment($scope.item.startsAt).format('YYYY-MM-DD');
      $scope.isNewItem = false;
    } else {
      $scope.item = {
        title: 'Trainer is working',
        startsAt: moment(moment().add(7, 'days').ceil(15, 'minutes').format('YYYY-MM-DD HH:mm')).toDate(),
        endsAt: moment(moment().add(7, 'days').add(1, 'hours').ceil(15, 'minutes').format('YYYY-MM-DD HH:mm')).toDate(),
        trainerId: trainer.id
      };
      $scope.isNewItem = true;
    }

    $scope.cancel = function () {
      $uibModalInstance.dismiss();
    };
    
    $scope.datepickerOptions = {
        showWeeks: false,
        startingDay: 'Monday'
    };
    
    $scope.toggleCalendar = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.calendarOpen = !$scope.calendarOpen;
    };
    
    $scope.returnItem = function(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'newAvailabilityForm');
        return false;
      }
      
      $scope.item.startsAt = new Date($scope.item.startsAt);
      $scope.item.endsAt = new Date($scope.item.endsAt);
      
      $scope.item.startsAt = moment($scope.item.date).add($scope.item.startsAt.getHours(), 'hours').add($scope.item.startsAt.getMinutes(), 'minutes').toDate();
      $scope.item.endsAt = moment($scope.item.date).add($scope.item.endsAt.getHours(), 'hours').add($scope.item.endsAt.getMinutes(), 'minutes').toDate();
      
      $uibModalInstance.close($scope.item);
    };
    
//    var newItem = {
//      title: 'Trainer is working', 
//      type: 'success', 
//      draggable: true, 
//      resizable: true,
//      startsAt: moment(moment().add(7, 'days').ceil(15, 'minutes').format('YYYY-MM-DD HH:mm')).toDate(),
//      endsAt: moment(moment().add(7, 'days').add(1, 'hours').ceil(15, 'minutes').format('YYYY-MM-DD HH:mm')).toDate(),
//      trainerId: $scope.user.trainer.id
//    };
}]);
