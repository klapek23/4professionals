'use strict';

angular.module('user.admin').controller('UserListController', ['$scope', '$filter', '$state', 'Admin', 'Trainers', 'notify', function($scope, $filter, $state, Admin, Trainers, notify) {

    Admin.query(function(data) {
      $scope.users = data;
      $scope.buildPager();
    });
    
    $scope.sortBy = 'createdAt';
    $scope.sortReverse = true;
    
    $scope.remove = function(user) {
      if (confirm('Are you sure you want to delete this user?')) {
        
        user.$remove({userId: user.id}).then(function() {
          $state.reload();
          notify({ classes: 'alert-success', message: 'User has been removed' });
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      }
    };

    $scope.buildPager = function() {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 20;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function() {
      $scope.filteredItems = $filter('filter')($scope.users, {
        $: $scope.search
      });
      
      if($scope.sortBy) {
        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortBy, $scope.sortReverse);
      }
      
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };
    
    $scope.sortTable = function(sortBy) {
      if(sortBy === $scope.sortBy) {
        $scope.sortReverse = !$scope.sortReverse;
      } else {
        $scope.sortReverse = false;
      }
      
      $scope.sortBy = sortBy;
      
      $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortBy, $scope.sortReverse);
      
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function() {
      $scope.figureOutItemsToDisplay();
    };
  }
]);