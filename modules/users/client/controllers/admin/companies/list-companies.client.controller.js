'use strict';

angular.module('user.admin').controller('CompaniesListController', ['$scope', '$filter', '$state', 'Company', 'notify', function($scope, $filter, $state, Company, notify) {

    Company.query(function(data) {
      $scope.companies = data;
      $scope.buildPager();
    });
    
    
    $scope.remove = function(company) {
      if (confirm('Are you sure you want to delete this company?')) {
        
        company.$remove({companyId: company.id}).then(function() {
          $state.reload();
          notify({ classes: 'alert-success', message: 'Company has been removed' });
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      }
    };

    $scope.buildPager = function() {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function() {
      $scope.filteredItems = $filter('filter')($scope.companies, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function() {
      $scope.figureOutItemsToDisplay();
    };
  }
]);