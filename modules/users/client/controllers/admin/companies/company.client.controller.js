'use strict';

angular.module('user.admin').controller('CompanyController', ['$scope', '$state', '$stateParams', '$location', 'Authentication', 'Company', 'notify', function($scope, $state, $stateParams, $location, Authentication, Company, notify) {
    
    $scope.authentication = Authentication;
    
    $scope.findOne = function() {
      $scope.company = Company.get({
        companyId: $stateParams.companyId
      });
    };
    
    $scope.create = function(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'companyForm');
        return false;
      }

      $scope.showSpinner = true;
      
      // Create new language object
      var company = new Company({
        name: this.company.name,
        address: this.company.address,
        email: this.company.email
      });

      // Redirect after save
      company.$save(function(response) {
        $scope.showSpinner = false;
        $state.go('admin.companies');
        notify({ classes: 'alert-success', message: 'New company has been added' });

        // Clear form fields
        $scope.company.name = '';
        $scope.company.address = '';
        $scope.company.email = '';
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };
    
    $scope.update = function(isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'companyForm');
        return false;
      }

      $scope.showSpinner = true;

      var company = $scope.company;

      company.$update({companyId: $scope.company.id}, function() {
        $scope.showSpinner = false;
        $state.go('admin.companies');
        notify({ classes: 'alert-success', message: 'Company has been updated' });
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };
}
]);