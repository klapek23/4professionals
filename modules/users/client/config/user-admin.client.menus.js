'use strict';

// Configuring the Articles module
angular.module('user.admin').run(['Menus',
  function(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage users',
      state: 'admin.users'
    });
    
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage clients permissions',
      state: 'admin.clients-permissions'
    });
    
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage companies',
      state: 'admin.companies'
    });
  }
]);