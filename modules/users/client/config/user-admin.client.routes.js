'use strict';

// Setting up route
angular.module('user.admin.routes').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider
      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/list-users.client.view.html',
        controller: 'UserListController'
      })
      .state('admin.user', {
        url: '/user/:userId',
        templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }],
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }],
          companiesResolve: ['$stateParams', 'Company', function($stateParams, Company) {
            return Company.query().$promise;
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.user-edit', {
        url: '/user/:userId/edit',
        templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }],
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }],
          companiesResolve: ['$stateParams', 'Company', function($stateParams, Company) {
            return Company.query().$promise;
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.user-create', {
        url: '/users/create',
        templateUrl: 'modules/users/client/views/admin/create-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: [function() {
            return {};
          }],
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }],
          companiesResolve: ['$stateParams', 'Company', function($stateParams, Company) {
            return Company.query().$promise;
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.user-trainer-availability', {
        url: '/user/:userId/availability',
        templateUrl: 'modules/users/client/views/admin/trainer-availability/view-availability.client.view.html',
        controller: 'TrainerAvailabilityController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            }).$promise;
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.clients-permissions', {
        url: '/clients/permissions',
        templateUrl: 'modules/users/client/views/admin/clients-permissions.client.view.html',
        controller: 'ClientsPermissionsController',
        resolve: {
          clientsResolve: ['$stateParams', 'Clients', function($stateParams, Clients) {
            return Clients.query().$promise;
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.companies', {
        url: '/companies',
        templateUrl: 'modules/users/client/views/admin/companies/list-companies.client.view.html',
        controller: 'CompaniesListController',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.company-create', {
        url: '/company/create',
        templateUrl: 'modules/users/client/views/admin/companies/create-company.client.view.html',
        controller: 'CompanyController',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.company-edit', {
        url: '/company/:companyId/edit',
        templateUrl: 'modules/users/client/views/admin/companies/edit-company.client.view.html',
        controller: 'CompanyController',
        data: {
          roles: ['admin']
        }
      });
  }
]);