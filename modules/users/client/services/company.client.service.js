'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('user').factory('Company', ['$resource',
  function($resource) {
    return $resource('api/company/:companyId', {
      companyId: '@companyId'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);