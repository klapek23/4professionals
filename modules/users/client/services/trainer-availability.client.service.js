'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('user').factory('TrainerAvailability', ['$resource',
  function($resource) {
    return $resource('api/trainer/:trainerId/trainer-availability/:availabilityId', {
      availabilityId: '@availabilityId',
      trainerId: '@trainerId'
    }, {
      update: {
        method: 'PUT'
      },
      getAvailableDays: {
        method: 'GET',
        url: 'api/trainer/:trainerId/trainer-available-days',
        isArray: true
      },
      getAvailableTimeByDate: {
        method: 'GET',
        url: 'api/trainer/:trainerId/trainer-availability-by-date',
        params: {
          dateFrom: '@dateFrom',
          dateTo: '@dateTo'
        },
        isArray: true
      }
    });
  }
]);


angular.module('user').factory('TrainerAvailabilities', function($http) {
    return {
      getAllAvailabilities: function() {
        return $http.get('api/trainers/trainers-availability').then(function(response) {
          return response.data;
        });
      },
      getAvailableDays: function() {
        return $http.get('api/trainers/available-days').then(function(response) {
          return response.data;
        });
      },
      getAvailableDaysByLanguage: function(data) {
        return $http.get('api/trainers/available-days/by-language',{
          params: {
            languageId: data.languageId
          }
        }).then(function(response) {
          return response.data;
        });
      },
      getAvailableTimeByDate: function(data) {
        return $http.get('api/trainers/trainer-availability/by-date', {
          params: {
            dateFrom: data.dateFrom,
            dateTo: data.dateTo
          }
        }).then(function(response) {
          return response.data;
        });
      }
    };
  }
);