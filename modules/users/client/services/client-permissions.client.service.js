'use strict';

angular.module('user').factory('ClientPermissions', function($http) {
    return {
      getAll: function() {
        return $http.get('api/admin/clients/permissions', {
          isArray: true
        }).then(function(response) {
          return response.data;
        });
      },
      getByClientId: function(clientId) {
        return $http.get('api/admin/clients/permissions/' + clientId).then(function(response) {
          return response.data;
        });
      },
      update: function(permissions) {
        return $http.post('api/admin/clients/permissions', {
          permissions: permissions
        }).then(function(response) {
          return response.data;
        });
      }
    };
  }
);