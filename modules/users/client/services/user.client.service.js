'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('user').factory('User', ['$resource',
  function($resource) {
    return $resource('api/user', {}, {
      get: {
        method: 'GET'
      },
      update: {
        method: 'PUT',
        params: {
          isAdmin: '@isAdmin'
        }
      }
    });
  }
]);

angular.module('user').factory('Trainers', ['$resource',
  function($resource) {
    return $resource('api/trainers', {}, {
      get: {
        method: 'GET',
        params: {
          id: '@id'
        }
      },
      getList: {
        method: 'GET',
        isArray: true
      },
      getByLanguage: {
        method: 'GET',
        url: 'api/trainers/by-language',
        params: {
          languageId: '@languageId'
        },
        isArray: true
      },
      getByDateAndLanguage: {
        method: 'GET',
        url: 'api/trainers/by-date-and-language',
        params: {
          languageId: '@languageId',
          date: '@date'
        },
        isArray: true
      }
    });
  }
]);

angular.module('user').factory('Clients', ['$resource',
  function($resource) {
    return $resource('api/admin/clients', {}, {
      get: {
        method: 'GET'
      }
    });
  }
]);

angular.module('user.admin').factory('Admin', ['$resource',
  function($resource) {
    return $resource('api/admin/user/:userId', {
      userId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
