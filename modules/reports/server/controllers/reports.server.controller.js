'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  json2xls = require('json2xls'),
  fs = require('fs'),
  moment = require('moment');

/**
 * Create report file and send back to user
 */
exports.generateReport = function(req, res) {
  var reportParams = {
    start_date: new Date(req.query.start_date),
    end_date: new Date(req.query.end_date),
    test_type: req.query.test_type,
    user_type: req.query.user_type,
    user_id: req.query.user_id,
    trainer: req.query.trainer
  };

  generateReport(reportParams);

  function generateReport(reportParams) {
    var query = {
      where: {
        createdAt: {
          $gte: moment(reportParams.start_date).format('YYYY-MM-DD'),
          $lte: moment(reportParams.end_date).add('day', 1).format('YYYY-MM-DD')
        }
      },
      include: [
        { model: db.trainer, include: [ db.user ] },
        { model: db.client, include: [ db.user, db.company ] },
        db.language, db[reportParams.test_type.slice(0, -1) + '_result'], db.candidate
      ]
    };

    if(reportParams.user_type === 'client') {
      query.where = Object.assign({}, query.where, {
        clientId: reportParams.user_id
      });
    }

    if(reportParams.user_type === 'trainer') {
      query.where = Object.assign({}, query.where, {
        trainerId: reportParams.user_id
      });
    }

    if(reportParams.trainer && reportParams.trainer !== 'null') {
      query.where = Object.assign({}, query.where, {
        trainerId: reportParams.trainer
      });
    }

    db[reportParams.test_type.slice(0, -1)].findAll(query).then(function(tests) {
      if(tests && tests.length > 0) {
        var reportTests = generateResults(tests, reportParams.test_type, reportParams.user_type);
        res.xls(reportParams.test_type + '_report.xlsx', reportTests);
      } else {
        res.send('No tests found');
      }
    });
  }

  function generateResults(tests, type, userType) {
    var reportTests = [];
    tests.forEach(function(test) {
      var basicData = {
        Status: test.status || '',
        Candidate: (test.candidate !== null ? test.candidate.name : 'not assigned'),
        CandidatePhone: (test.candidate && test.candidate.phone !== null ? test.candidate.phone : '-'),
        Language: (test.language !== null ? test.language.title : 'not assigned')
      },
      completeData = {};

      if(userType === 'admin') {
        if(type === 'speaking_tests') {
          completeData = Object.assign({}, basicData, {
            Company: (test.client !== null && test.client.company !== null ? test.client.company.name : 'not assigned'),
            Trainer: (test.trainer !== null && test.trainer.user !== null ? test.trainer.user.username : 'not assigned'),
            Level: test.level,
            StartsAt: moment(test.startsAt).format('YYYY-MM-DD HH:mm'),
            EndsAt: moment(test.endsAt).format('YYYY-MM-DD HH:mm')
          });
        } else if(type === 'writing_tests') {
          completeData = Object.assign({}, basicData, {
            Company: (test.client !== null && test.client.company !== null ? test.client.company.name : 'not assigned'),
            Trainer: (test.trainer !== null && test.trainer.user !== null ? test.trainer.user.username : 'not assigned')
          });
        }
      } else if (userType === 'client' || userType === 'trainer') {
        if(type === 'speaking_tests') {
          completeData = Object.assign({}, basicData, {
            Level: test.level,
            StartsAt: moment(test.startsAt).format('YYYY-MM-DD HH:mm'),
            EndsAt: moment(test.endsAt).format('YYYY-MM-DD HH:mm')
          });
        } else if(type === 'writing_tests') {
          completeData = Object.assign({}, basicData, {
            Company: (test.client !== null && test.client.company !== null ? test.client.company.name : 'not assigned'),
            Trainer: (test.trainer !== null && test.trainer.user !== null ? test.trainer.user.username : 'not assigned')
          });
        }
      }

      completeData.CreatedAt = moment(test.createdAt).format('YYYY-MM-DD HH:mm');

      reportTests.push(completeData);
    });

    return reportTests;
  }
};
