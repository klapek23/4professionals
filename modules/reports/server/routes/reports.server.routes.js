'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  reportsPolicy = require('../policies/reports.server.policy'),
  reports = require(path.resolve('./modules/reports/server/controllers/reports.server.controller')),
  json2xls = require('json2xls');


module.exports = function(app) {

  // Reports collection routes
  app.route('/api/reports/generate')
    .all(reportsPolicy.isAllowed)
    .get(json2xls.middleware, reports.generateReport);
};
