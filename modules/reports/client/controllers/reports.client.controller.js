'use strict';

// Languages controller
angular.module('reports').controller('ReportsController', ['$scope', '$http', '$state', '$stateParams', '$location', 'Authentication', 'Reports', 'notify', 'moment', 'Trainers', function($scope, $http, $state, $stateParams, $location, Authentication, Reports, notify, moment, Trainers) {
    $scope.authentication = Authentication;
    $scope.TrainersService = Trainers;
    $scope.report = {};
    $scope.test_types = [
      {
        val: 'speaking_tests',
        name: 'speaking tests'
      }, {
        val: 'writing_tests',
        name: 'writing tests'
      }/*, {
        val: 'all',
        name: 'all'
      }*/];
    
    $scope.datepickerStartDateOptions = {
      showWeeks: false,
      startingDay: 'Monday',
      opened: false,
      maxDate: moment(),
      toggleCalendar: function() {
        this.opened = !this.opened;
      }
    };
    
    $scope.datepickerEndDateOptions = {
      showWeeks: false,
      startingDay: 'Monday',
      opened: false,
      maxDate: moment(),
      toggleCalendar: function() {
        this.opened = !this.opened;
      }
    };

    $scope.toggleSearchByTrainer = function() {
      if($scope.report.generate_by_trainer && !$scope.trainers) {
        $scope.TrainersService.getList().$promise.then(function(response) {
          $scope.trainers = response;
        });
      }
    };
    

    // Generate report
    $scope.generateReport = function(isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'reportForm');
        return false;
      }
      
      var urlParams = {
        start_date: moment($scope.report.start_date).format('YYYY-MM-DD'),
        end_date: moment($scope.report.end_date).format('YYYY-MM-DD'),
        test_type: $scope.report.test_type.val,
        user_type: $scope.authentication.user.roles,
        user_id: getUserId(),
        trainer: $scope.report.generate_by_trainer && $scope.report.trainer ? $scope.report.trainer : null
      };

      function getUserId() {
        if($scope.authentication.user.hasOwnProperty('client') && $scope.authentication.user.client !== null) {
          return $scope.authentication.user.client.id;
        } else if($scope.authentication.user.hasOwnProperty('trainer') && $scope.authentication.user.trainer !== null) {
          return $scope.authentication.user.trainer.id;
        } else {
          return null;
        }
      }
      
      window.open('/api/reports/generate?start_date=' + urlParams.start_date + '&end_date=' + urlParams.end_date + '&test_type=' + urlParams.test_type + '&user_type=' + urlParams.user_type + '&user_id=' + urlParams.user_id + '&trainer=' + urlParams.trainer, '_blank');
    };
  }
]);
