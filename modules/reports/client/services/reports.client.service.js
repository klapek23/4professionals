'use strict';

// Reports service used for communicating with the users REST endpoint
angular.module('reports').factory('Reports', function($http) {
    return {
      generateReport: function(params) {
        console.log(params);
        
        return $http({
          method: 'GET',
          url: '/api/reports/generate',
          params: params
        }).then(function(response) {
          return response.data;
        }, function(response) {
          console.log(response);
        });
      }
    };
  }
);