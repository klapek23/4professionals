'use strict';

// Setting up route
angular.module('reports').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
      .state('reports', {
        url: '/reports',
        abstract: true,
        template: '<ui-view class="">',
        data: {
          roles: ['admin', 'client', 'trainer']
        }
      })
      .state('reports.generate', {
        url: '',
        templateUrl: 'modules/reports/client/views/generate-report.client.view.html',
        controller: 'ReportsController',
        data: {
          roles: ['admin', 'client', 'trainer']
        }
      });
  }
]);
