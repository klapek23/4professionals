'use strict';

// Configuring the Languages module
angular.module('reports').run(['Menus',
  function(Menus) {
    
    Menus.addMenuItem('topbar', {
      title: 'Reports',
      state: 'reports.generate',
      roles: ['admin', 'client', 'trainer'],
      icon: '<i class="fa fa-file-excel-o"></i>',
      position: 4
    });
  }
]);
