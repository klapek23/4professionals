'use strict';

// Writing tests fill controller
angular.module('writing_tests').controller('Writing_testFillController', ['$scope', '$stateParams', '$location', 'Writing_test', 'Writing_testResult', '$state', 'moment', function($scope, $stateParams, $location, Writing_test, Writing_testResult, $state, moment) {
    
    $scope.errors = false;
    $scope.token = $stateParams.token;
    $scope.result = {};
    
    if($state.current.name !== 'writing_tests.fill_success') {
      Writing_test.getByToken({token: $scope.token}).$promise.then(function(test) {
        if(!test) {
          $state.go('not-found');
        } else {
          $scope.test = test;
          $scope.$broadcast('test-ready', $scope.test);
        }
      }, function(err) {
        $scope.errors = [
          {
            key: 'token',
            message: err.data.message
          }
        ];
      });
    }
    
    
    $scope.sendFormResult = function(isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'writing_testCandidateForm');
        return false;
      }

      $scope.showSpinner = true;
      
      var result = new Writing_testResult($scope.result);
      
      result.$save({writing_testId: $scope.test.testId}).then(function(result) {
        
        Writing_test.updateStatus({writing_testId: $scope.test.testId}, {
          status: 'filled',
          activeTo: moment().format('YYYY-MM-DD HH:mm')
        }).$promise.then(function(test) {
          $state.go('writing_tests.fill_success', {token: $stateParams.token});
          $scope.showSpinner = false;
        }).catch(function(err) {
          $scope.errors = [
            {
              key: 'test',
              message: err.data.message
            }
          ];
          $scope.showSpinner = false;
        });
      }).catch(function(err) {
        $scope.errors = [
          {
            key: 'test',
            message: err.data.message
          }
        ];
        $scope.showSpinner = false;
      });
    };


    $scope.sendPartialFormResult = function() {
      var result = new Writing_testResult($scope.result);
      result.status = 'pending';

      result.$save({writing_testId: $scope.test.testId});
    };
  }
]);
