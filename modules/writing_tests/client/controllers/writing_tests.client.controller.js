'use strict';

// Speaking tests controller
angular.module('writing_tests').controller('Writing_testController', ['$scope', '$stateParams', '$location', 'Authentication', 'languagesResolve', 'Writing_test', 'Writing_testPattern', 'Writing_testResult', 'Trainers', '$state', '$filter', 'notify', 'moment','Clients', '$window', 'FileUploader', 'Socket', function($scope, $stateParams, $location, Authentication, languagesResolve, Writing_test, Writing_testPattern, Writing_testResult, Trainers,  $state, $filter, notify, moment, Clients, $window, FileUploader, Socket) {
  $scope.showSpinner = false;

    if($state.current.name !== 'writing_tests.fill' || $state.current.name !== 'writing_tests.fill_success') {
      $scope.authentication = Authentication;
      $scope.languages = languagesResolve;

      $scope.statusses = ['waiting for trainer', 'canceled', 'done'];
      $scope.levels = ['A1', 'A1 25%', 'A1 50%', 'A1 75%', 'A2', 'A2 25%', 'A2 50%', 'A2 75%', 'B1', 'B1 25%', 'B1 50%', 'B1 75%', 'B2', 'B2 25%', 'B2 50%', 'B2 75%', 'C1', 'C1 25%', 'C1 50%', 'C1 75%', 'C2', 'N/A'];
    }


    moment.locale('pl', {
      week : {
        dow : 1 // Monday is the first day of the week
      }
    });

    if($scope.authentication.user.roles === 'admin') {
      Clients.query().$promise.then(function(clients) {
        angular.forEach(clients, function(client, i) {
          $scope.clients = [];

          angular.forEach(clients, function(client, i) {
            if(client.hasOwnProperty('client_permission') && client.client_permission.writing_tests === true) {
              client.nameWithCompany = client.user.displayName + ' - ' + client.company.name;
              $scope.clients.push(client);
            }
          });
        });
      }, function(err) {
        console.log(err);
      });
    }

    $scope.languageChange = function() {
      Trainers.getByLanguage({languageId: $scope.test.language.id}).$promise.then(function(trainers) {
        angular.forEach(trainers, function(trainer, i) {
          trainer.nameWithEmail = trainer.user.displayName + ' - ' + trainer.user.email;
        });

        $scope.test.trainersByLanguage = trainers;
        $scope.test.trainerDisabled = false;
      }, function(err) {
        console.log(err);
      });

      if ($scope.authentication.user.roles === 'client') {
        Writing_testPattern.getByLanguageAndCompany({
          languageId: $scope.test.language.id,
          companyId: $scope.authentication.user.client.company.id
        }).$promise.then(function(patterns) {
          $scope.test.patterns = patterns;
          $scope.test.patternDisabled = false;
          delete $scope.test.pattern;
        });
      } else if ($scope.authentication.user.roles === 'admin') {
        Writing_testPattern.getByLanguage({languageId: $scope.test.language.id}).$promise.then(function(patterns) {
          $scope.test.patterns = patterns;
          $scope.test.patternDisabled = false;
          delete $scope.test.pattern;
        }, function(err) {
          console.log(err);
        });
      }
    };

    $scope.prepareForm = function() {
      $scope.test = {};
      $scope.test.trainerDisabled = true;
      $scope.test.patternDisabled = true;

      $scope.test.ttlOptions = [
        {
          key: '20 seconds',
          value: 20,
          unit: 'seconds',
          expires: '20'
        },
        {
          key: '1 hour',
          value: 1,
          unit: 'hours',
          expires: '1h'
        },
        {
          key: '3 hours',
          value: 3,
          unit: 'hours',
          expires: '3h'
        },
        {
          key: '1 day',
          value: 1,
          unit: 'days',
          expires: '1d'
        },
        {
          key: 'unlimited',
          value: 0,
          unit: 'minutes',
          expires: false
        }
      ];
    };


    $scope.create = function(isValid) {
      $scope.error = null;
      $scope.submitted = true;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'writing_testForm');
        return false;
      }

      $scope.showSpinner = true;

      if(this.test.ttl.value > 0) {
        this.test.expiresAt = moment().add(this.test.ttl.value, this.test.ttl.unit).format('YYYY-MM-DD HH:mm');
      } else {
        this.test.expiresAt = null;
      }

      var test = new Writing_test({
        notes: this.test.notes,
        expiresAt: this.test.expiresAt,
        candidateFullName: this.test.candidateFullName,
        //candidatePhone: this.test.candidatePhone,
        candidateEmail: this.test.candidateEmail,
        send_to_company: $scope.authentication.user.roles.indexOf('client') !== -1 && $scope.authentication.user.client.company.id === 5 ? true : this.test.send_to_company,
        languageId: this.test.language.id,
        ttl: this.test.ttl,
        writingTestPatternId: this.test.pattern.id,
        clientId: ($scope.authentication.user.roles !== 'admin' ? $scope.authentication.user.client.id : this.test.client.id),
        companyId: ($scope.authentication.user.roles !== 'admin' ? $scope.authentication.user.client.company.id : this.test.client.company.id)
      });

      test.$save().then(function(test) {
        $scope.showSpinner = false;
        $location.path('/writing_tests');
        notify({ classes: 'alert-success', message: 'New writing test has been added' });
      }, function(err) {
        $scope.showSpinner = false;
        notify({ classes: 'alert-danger', message: 'Error when adding new test: ' + err.message });
      });
    };


    $scope.update = function(isValid) {
      $scope.error = null;
      $scope.submitted = true;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'writing_testForm');
        return false;
      }

      $scope.showSpinner = true;

      var test = $scope.test;
      test.clientId = ($scope.authentication.user.roles !== 'admin' ? $scope.authentication.user.client.id : this.test.client.id);
      test.companyId = ($scope.authentication.user.roles !== 'admin' ? $scope.authentication.user.client.company.id : this.test.client.company.id);

      test.$update().then(function(test) {
        $scope.showSpinner = false;
        $location.path('/writing_tests');
        notify({ classes: 'alert-success', message: 'Writing test has been updated' });
      }, function(err) {
        $scope.showSpinner = false;
        console.log(err);
        notify({ classes: 'alert-danger', message: 'Error when updating test: ' + err.message });
      });
    };


    $scope.remove = function(test) {
      if (test) {
        test.$remove();
        if($location.$$path !== '/writing_tests') {
          $location.path('writing_tests');
        } else {
          $state.reload();
        }
        notify({ classes: 'alert-success', message: 'Writing test has been removed' });
      } else {
        $scope.test.$remove(function() {
          if($location.$$path !== '/writing_tests') {
            $location.path('writing_tests');
          } else {
            $state.reload();
          }

          notify({ classes: 'alert-success', message: 'Writing test has been removed' });
        });
      }
    };


    $scope.findOne = function() {
      Writing_test.get({testId: $stateParams.testId}).$promise.then(function(test) {
        $scope.test = test;
        $scope.showResults = true;
        $scope.showUpdateButtonValue = false;

        $scope.test.candidateFullName = test.candidate.name;
        //$scope.test.candidatePhone = test.candidate.phone;
        $scope.test.candidateEmail = test.candidate.email;

        Trainers.getByLanguage({languageId: $scope.test.language.id}).$promise.then(function(trainers) {
          angular.forEach(trainers, function(trainer, i) {
            trainer.nameWithEmail = trainer.user.displayName + ' - ' + trainer.user.email;
          });

          $scope.test.trainersByLanguage = trainers;
          $scope.test.trainerDisabled = false;
          $scope.trainers = trainers;

          if ($scope.authentication.user.roles === 'client') {
            Writing_testPattern.getByLanguageAndCompany({
              languageId: $scope.test.language.id,
              companyId: $scope.authentication.user.client.company.id
            }).$promise.then(function(patterns) {
              $scope.test.patterns = patterns;
              $scope.test.patternDisabled = false;
              delete $scope.test.pattern;
            }, function(err) {
              console.log(err);
            });
          } else if ($scope.authentication.user.roles === 'admin') {
            Writing_testPattern.getByLanguage({languageId: $scope.test.language.id}).$promise.then(function(patterns) {
              $scope.test.patterns = patterns;
              $scope.test.patternDisabled = false;
              delete $scope.test.pattern;
            }, function(err) {
              console.log(err);
            });
          }
        }, function(err) {
          console.log(err);
        });

      }, function(err) {
        console.log(err);
      });
    };


    $scope.writing_testUpdateStatus = function(isValid) {

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'writing_testUpdateForm');
        return false;
      }

      $scope.showSpinner = true;

      Writing_test.updateStatus({writing_testId: $scope.test.id}, {
        status: $scope.test.status,
        level: $scope.test.level,
        trainerId: ($scope.test.hasOwnProperty('trainer') && $scope.test.trainer !== null ? $scope.test.trainer.id : null),
        notes: $scope.test.notes
      }).$promise.then(function(test) {
        $scope.showSpinner = false;
        $state.go('writing_tests.list');
        notify({ classes: 'alert-success', message: 'Test status update successfull' });
      }, function(err) {
        $scope.showSpinner = false;
        notify({ classes: 'alert-danger', message: 'Error when updating test: ' + err.message });
      });
    };

    $scope.writing_testUpdateStatusByTrainer = function() {
      $scope.showSpinner = true;
      Writing_test.updateStatus({writing_testId: $scope.test.id}, {
        level: $scope.test.level,
        status: 'done'
      }).$promise.then(function(test) {
        Writing_testResult.get({writing_testId: $scope.test.id}, function(result) {
          result.answers = $scope.test.writing_test_result.answers;
          result.$update({writing_testId: $scope.test.id},function(result) {
            $scope.showSpinner = false;
            $state.go('writing_tests.list');
            notify({ classes: 'alert-success', message: 'Test status update successfull' });
          });
        });
      }, function(err) {
        $scope.showSpinner = false;
        notify({ classes: 'alert-danger', message: 'Error when updating test: ' + err.message });
      });
    };


    $scope.toggleResults = function(status) {
      $scope.showResults = status;
    };

    $scope.showUpdateButton = function() {
      $scope.showUpdateButtonValue = true;
    };
  }
]);
