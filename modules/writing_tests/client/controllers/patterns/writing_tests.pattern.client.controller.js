'use strict';

// Speaking tests controller
angular.module('writing_tests').controller('Writing_testPatternController', ['$scope', '$stateParams', '$location', 'Authentication', 'languagesResolve', 'Writing_testPattern', 'companiesResolve', '$state', '$filter', 'notify', '$window', function($scope, $stateParams, $location, Authentication, languagesResolve,  Writing_testPattern, companiesResolve, $state, $filter, notify, $window) {
    $scope.authentication = Authentication;
    $scope.languages = languagesResolve;
    $scope.pattern = {};
    $scope.showSpinner = false;
    
    $scope.prepareForm = function() {
      $scope.pattern.questions = [];
      $scope.companies = companiesResolve;
    };
    
    var QuestionPatterns = {
      removeQuestion: function(question) {
        var index = $scope.pattern.questions.indexOf(question);
        $scope.pattern.questions.splice(index, 1);
      },
      radio: function(id, label, correct, answers) {
        this.id = id;
        this.type = 'radio';
        this.label = label || '';
        this.name = convertToSlug(this.label);
        this.answers = answers || [
          {
            id: 'answer_0',
            val: 'example option'
          }
        ];
        this.correct = correct || 0;
        
        this.addAnswer = function(id) {
          var answer = {
            id: 'answer_' + id,
            val: 'new option'
          };
          
          this.answers.push(answer);
        };
        
        this.removeAnswer = function(answer) {
          var index = this.answers.indexOf(answer);
          this.answers.splice(index, 1);
        };
        
        this.setActiveAnswer = function(answerIndex) {
          this.correct = answerIndex;
        };
      },
      text: function(id, label, placeholder) {
        this.id = id;
        this.type = 'text';
        this.label = label || '';
        this.name = convertToSlug(this.label);
        this.placeholder = placeholder || '';
      }
    };
    
    QuestionPatterns.radio.prototype = QuestionPatterns;
    QuestionPatterns.text.prototype = QuestionPatterns;
    
    $scope.addQuestion = function(type, params) {
      var question,
          questionID;
  
      if($scope.pattern.questions[$scope.pattern.questions.length-1]) {
        questionID = $scope.pattern.questions[$scope.pattern.questions.length-1].id;
        questionID = questionID.replace(/q_/g, '');
        questionID = parseInt(questionID, 10);
        questionID = questionID + 1;
        questionID = 'q_' + questionID;
      } else {
        questionID = 'q_0';
      }
  
      if(type === 'radio') {
        question = new QuestionPatterns.radio(questionID, params.label || '', params.correct || 0, params.answers || null);
      } else if(type === 'text') {
        question = new QuestionPatterns.text(questionID, params.label || '', params.placeholder  || '');
      }
      
      $scope.pattern.questions.push(question);
    };
    
    $scope.removeQuestion = QuestionPatterns.removeQuestion;
    $scope.addAnswer = QuestionPatterns.radio.addAnswer;
    $scope.removeAnswer = QuestionPatterns.radio.removeAnswer;
    
    
    $scope.create = function(isValid) {
      $scope.error = null;
      $scope.submitted = true;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'writing_testPatternForm');
        return false;
      }

      $scope.showSpinner = true;
      
      angular.forEach($scope.pattern.questions, function(question, i) {
        $scope.pattern.questions[i].id = 'q_' + i;
      });
      
      var pattern = new Writing_testPattern({
        name: $scope.pattern.name,
        language: $scope.pattern.language,
        companies: $scope.pattern.companies,
        questions: $scope.pattern.questions
      });
      
      pattern.$save().then(function(pattern) {
        $scope.showSpinner = false;
        $location.path('/writing_tests_patterns');
        notify({ classes: 'alert-success', message: 'New writing test pattern has been added' });
      }, function(err) {
        $scope.showSpinner = false;
        notify({ classes: 'alert-danger', message: 'Error when adding new pattern: ' + err.message });
      });
    };
    
    
    $scope.update = function(isValid) {
      $scope.error = null;
      $scope.submitted = true;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'writing_testPatternForm');
        return false;
      }

      $scope.showSpinner = true;
      
      angular.forEach($scope.pattern.questions, function(question, i) {
        $scope.pattern.questions[i].id = 'q_' + i;
      });
      
      var pattern = $scope.pattern;

      pattern.$update(function() {
        $scope.showSpinner = false;
        $location.path('/writing_tests_patterns');
        notify({ classes: 'alert-success', message: 'Writing test pattern has been updated' });
      }, function(errorResponse) {
        $scope.showSpinner = false;
        $scope.error = errorResponse.data.message;
      });
    };
    
    
    $scope.delete = function(pattern) {
      if (pattern) {
        pattern.$remove().then(function() {
          notify({ classes: 'alert-success', message: 'Writing test pattern has been removed' });
          $state.reload();
        }, function(err) {
          notify({ classes: 'alert-danger', message: 'Something went wrong:' + err.message });
        });
      }
    };
    
    $scope.findOne = function() {
      Writing_testPattern.get({patternId: $stateParams.patternId}).$promise.then(function(pattern) {
        angular.forEach(pattern.questions, function(question, i) {
          if(question.type === 'radio') {
            pattern.questions[i] = new QuestionPatterns.radio(question.id, question.label || '', question.correct, question.answers || null);
          } else if(question.type === 'text') {
            pattern.questions[i] = new QuestionPatterns.text(question.id, question.label || '', question.placeholder  || '');
          }
        });
        
        $scope.pattern = pattern;
      }, function(err) {
        console.log(err);
      });
    };
    
    function convertToSlug(Text) {
      return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'');
    }
  }
]);