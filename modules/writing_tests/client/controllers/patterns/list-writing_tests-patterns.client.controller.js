'use strict';

// Speaking tests controller
angular.module('writing_tests').controller('ListWriting_testPatternController', ['$scope', '$stateParams', '$location', 'Authentication', 'Writing_testPattern', '$state', '$filter', 'notify', 'FileUploader', function($scope, $stateParams, $location, Authentication, Writing_testPattern, $state, $filter, notify, FileUploader) {
    $scope.authentication = Authentication;
    
    Writing_testPattern.query().$promise.then(function(patterns) {
      angular.forEach(patterns, function(pattern, i) {
        var countRadio = 0,
            countText = 0;
            
        angular.forEach(pattern.questions, function(question, index) {
          if(question.type === 'radio') { countRadio++; }
          else if(question.type === 'text') { countText++; }
        });
        
        patterns[i].countRadio = countRadio;
        patterns[i].countText = countText;
      });
      
      $scope.patterns = patterns;
      
      $scope.buildPager();
    });
    
    
    // Remove existing Test
    $scope.delete = function(pattern) {
      if (pattern) {
        pattern.$remove().then(function() {
          notify({ classes: 'alert-success', message: 'Writing test pattern has been removed' });
          $state.reload();
        });        
      }
    };
    
    $scope.buildPager = function() {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 12;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function() {
      $scope.filteredItems = $filter('filter')($scope.patterns, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function() {
      $scope.figureOutItemsToDisplay();
    };
    
    $scope.getAllTests = function() {
      $scope.patterns = Writing_testPattern.query(function() {
        $scope.buildPager();
      });
    };
    
    $scope.getFutureTests = function() {
      $scope.patterns = Writing_testPattern.getRunningTests(function() {
        $scope.buildPager();
      });
    };
    
    
    //Result files functions
    
    // Create file uploader instance
//    $scope.createUploader = function(test) {
//      var uploader = new FileUploader({
//        url: 'api/writing_tests/results/test/' + test.id,
//        alias: 'writingTestResult',
//        autoUpload: true
//      });
//    
//      // Set file uploader file filter
//      uploader.filters.push({
//        name: 'fileFilter',
//        fn: function(item, options) {
//          var aName = item.name.split('.');
//          if(['doc', 'docx', 'pdf', 'xls', 'xlsx', 'odt', 'txt'].indexOf(aName[aName.length - 1]) !== -1) {
//            return true;
//          } else {
//            return false;
//          }
//        }
//      });
//      
//      uploader.onSuccessItem = function(item, response, status, headers) {
//        notify({ classes: 'alert-success', message: 'Writing test result has been added' });
//        $state.reload();
//      };
//      
//      uploader.onErrorItem = function(item, response, status, headers) {
//        notify({ classes: 'alert-danger', message: 'Writing test result uploading error' });
//      };
//      
//      return uploader;
//    };
  }
]);