'use strict';

angular.module('writing_tests')
  .directive('testDowntime', ['$interval', '$timeout', '$state', 'moment', function($interval, $timeout, $state, moment) {
    return {
      restrict: 'E',
      templateUrl: '/modules/writing_tests/client/views/directives/test-downtime.html',
      link: function (scope, el, attrs) {
        scope.getTimeRemaining = function(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor( (t/1000) % 60 );
          var minutes = Math.floor( (t/1000/60) % 60 );
          var hours = Math.floor( (t/(1000*60*60)) % 24 );
          var days = Math.floor( t/(1000*60*60*24) );
          return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,
            'display': '0' + hours + ':' + ('0' + minutes).slice(-2) + ':' + ('0' + seconds).slice(-2)
          };
        };

        scope.initializeClock = function(endtime) {
          scope.timeinterval = $interval(function() {
            scope.timeLeft = scope.getTimeRemaining(endtime);
            el.addClass('active');
            if(scope.timeLeft.total<=0){
              $interval.cancel(scope.timeinterval);
              window.location.href = '/authentication/signin';
            }
          },1000);
        };

        //start counter
        scope.$on('test-ready', function(event, test) {
          scope.time = test.activeTo;
          scope.expires = moment(scope.time).format('YYYY-MM-DD HH:mm:ss');
          scope.initializeClock(scope.time);

          scope.$watch('timeLeft', function(newValue) {
            var sendResultHours = [
              '00:45:00',
              '00:30:00',
              '00:15:00'
            ];

            if(newValue && sendResultHours.indexOf(newValue.display) > -1) {
              scope.sendPartialFormResult();
            }
          });
        });
      }
    };
  }]);

