'use strict';

angular.module('writing_tests').run(['Menus', 
  function(Menus) {
    // Add the writing tests dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Writing tests',
      state: 'writing_tests',
      type: 'dropdown',
      roles: ['admin', 'client', 'trainer'],
      icon:  '<i class="fa fa-pencil-square-o"></i>',
      position: 2
    });
    
    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'writing_tests', {
      title: 'Test list',
      state: 'writing_tests.list',
      //roles: ['admin', 'trainer', 'client']
      roles: ['admin', 'client', 'trainer']
    });
    
    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'writing_tests', {
      title: 'Create a test',
      state: 'writing_tests.create',
      //roles: ['admin', 'client']
      roles: ['admin', 'client']
    });
    
    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'writing_tests', {
      title: 'List test patterns',
      state: 'writing_tests_patterns.list',
      roles: ['admin']
    });
    
    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'writing_tests', {
      title: 'Create a pattern',
      state: 'writing_tests_patterns.create',
      roles: ['admin']
    });
}]);