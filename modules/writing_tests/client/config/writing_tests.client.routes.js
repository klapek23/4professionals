'use strict';

// Setting up route
angular.module('writing_tests').config(['$stateProvider',
  function($stateProvider) {
    
    $stateProvider
      .state('writing_tests', {
        url: '/writing_tests',
        abstract: true,
        template: '<ui-view class="">'
      })
      .state('writing_tests.list', {
        url: '',
        templateUrl: 'modules/writing_tests/client/views/list-writing_tests.client.view.html',
        controller: 'ListWriting_testController',
        data: {
          roles: ['admin', 'trainer', 'client']
        }
      })
      .state('writing_tests.create', {
        url: '/create',
        templateUrl: 'modules/writing_tests/client/views/create-writing_tests.client.view.html',
        controller: 'Writing_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }]
        },
        data: {
          roles: ['admin', 'client']
        }
      })
      .state('writing_tests.edit', {
        url: '/:testId/edit',
        templateUrl: 'modules/writing_tests/client/views/edit-writing_tests.client.view.html',
        controller: 'Writing_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query().$promise;
          }]
        },
        data: {
          roles: ['admin', 'client']
        }
      })
      .state('writing_tests.view', {
        url: '/:testId',
        templateUrl: 'modules/writing_tests/client/views/view-writing_tests.client.view.html',
        controller: 'Writing_testController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query();
          }]
        },
        data: {
          roles: ['admin', 'trainer', 'client']
        }
      })
      .state('writing_tests.fill', {
        url: '/fill/:token',
        templateUrl: 'modules/writing_tests/client/views/fill/fill-writing_tests.client.view.html',
        controller: 'Writing_testFillController'
      })
      .state('writing_tests.fill_success', {
        url: '/fill/success/:token',
        templateUrl: 'modules/writing_tests/client/views/fill/fill-success-writing_tests.client.view.html',
        controller: 'Writing_testFillController'
      })
      .state('writing_tests_patterns', {
        url: '/writing_tests_patterns',
        abstract: true,
        template: '<ui-view class="writing-tests">'
      })
      .state('writing_tests_patterns.list', {
        url: '',
        templateUrl: 'modules/writing_tests/client/views/patterns/list-writing_test-patterns.client.view.html',
        controller: 'ListWriting_testPatternController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query();
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('writing_tests_patterns.create', {
        url: '/create',
        templateUrl: 'modules/writing_tests/client/views/patterns/create-writing_tests-pattern.client.view.html',
        controller: 'Writing_testPatternController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query();
          }],
          companiesResolve: ['$stateParams', 'Company', function($stateParams, Company) {
            return Company.query().$promise;
          }]
        },
        data: {
          roles: ['admin']
        }
      })
      .state('writing_tests_patterns.edit', {
        url: '/:patternId/edit',
        templateUrl: 'modules/writing_tests/client/views/patterns/edit-writing_tests-pattern.client.view.html',
        controller: 'Writing_testPatternController',
        resolve: {
          languagesResolve: ['$stateParams', 'Languages', function($stateParams, Languages) {
            return Languages.query();
          }],
          companiesResolve: ['$stateParams', 'Company', function($stateParams, Company) {
            return Company.query();
          }]
        },
        data: {
          roles: ['admin']
        }
      });
  }
]);