'use strict';

// Speaking tests service used for communicating with the users REST endpoint
angular.module('writing_tests').factory('Writing_testResult', ['$resource',
  function($resource) {
    return $resource('api/writing_tests/:writing_testId/result', {
      writing_testId: '@writing_testId'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);