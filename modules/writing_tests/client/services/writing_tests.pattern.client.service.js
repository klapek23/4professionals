'use strict';

// Speaking tests service used for communicating with the users REST endpoint
angular.module('writing_tests').factory('Writing_testPattern', ['$resource',
  function($resource) {
    return $resource('api/writing_tests/patterns/:patternId', {
      patternId: '@id'
    }, {
      update: {
        method: 'PUT'
      },
      getByLanguage: {
        method: 'GET',
        url: 'api/writing_tests/patterns/get/by-language/:languageId',
        isArray: true,
        params: {
          languageId: '@languageId'
        }
      },
      getByLanguageAndCompany: {
        method: 'GET',
        url: 'api/writing_tests/patterns/get/by-language/:languageId/company/:companyId',
        isArray: true,
        params: {
          languageId: '@languageId',
          companyId: '@companyId'
        }
      }
    });
  }
]);