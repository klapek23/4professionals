'use strict';

// Speaking tests service used for communicating with the users REST endpoint
angular.module('writing_tests').factory('Writing_test', ['$resource',
  function($resource) {
    return $resource('api/writing_tests/:testId', {
      testId: '@id'
    }, {
      update: {
        method: 'PUT'
      },
      updateStatus: {
        method: 'PUT',
        url: 'api/writing_tests/update/by-id/:writing_testId',
        params: {
          writing_testId: '@writing_testId'
        }
      },
      getRunningTests: {
        method: 'GET',
        url: 'api/writing_tests/get/running-tests',
        isArray: true
      },
      getTestsByCompany: {
        method: 'GET',
        url: 'api/writing_tests/get/by-company/:companyId',
        params: {
          clientId: '@companyId'
        },
        isArray: true
      },
      getTestsByClient: {
        method: 'GET',
        url: 'api/writing_tests/get/by-client/:clientId',
        params: {
          clientId: '@clientId'
        },
        isArray: true
      },
      getTestsByTrainer: {
        method: 'GET',
        url: 'api/writing_tests/get/by-trainer/:trainerId',
        params: {
          trainerId: 'trainerId'
        },
        isArray: true
      },
      getTestsByDateTrainer: {
        method: 'GET',
        url: 'api/writing_tests/get/by-date-trainer',
        params: {
          dateFrom: '@dateFrom',
          dateTo: '@dateTo',
          trainerId: '@trainerId'
        },
        isArray: true
      },
      getByToken: {
        method: 'GET',
        url: 'api/writing_tests/get/by-token/:token',
        params: {
          token: '@token'
        }
      }
    });
  }
]);