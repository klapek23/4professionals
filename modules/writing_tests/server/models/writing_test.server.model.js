"use strict";

var crypto = require('crypto'),
    moment = require('moment');

module.exports = function(sequelize, DataTypes) {

  var Writing_test = sequelize.define('writing_test', {
    status: {
      type: DataTypes.ENUM('pending', 'filled', 'waiting for trainer', 'canceled', 'done'),
      defaultValue: 'pending'
    },
    level: {
      type: DataTypes.ENUM('A1', 'A1 25%', 'A1 50%', 'A1 75%', 'A2', 'A2 25%', 'A2 50%', 'A2 75%', 'B1', 'B1 25%', 'B1 50%', 'B1 75%', 'B2', 'B2 25%', 'B2 50%', 'B2 75%', 'C1', 'C1 25%', 'C1 50%', 'C1 75%', 'C2', 'N/A')
    },
    salt: DataTypes.STRING,
    password: DataTypes.STRING,
    send_to_company: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    expiresAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    token: {
      type: DataTypes.TEXT,
      default: null
    },
    notes: DataTypes.TEXT,
    startedAt: {
      type: DataTypes.DATE,
      default: null,
      allowNull: true
    },
    activeTo: {
      type: DataTypes.DATE,
      default: null,
      allowNull: true
    }
  }, {
    instanceMethods: {
      makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
      },
      encryptPassword: function(password, salt) {
        if (!password || !salt)
          return '';
        salt = new Buffer(salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha256').toString('base64');
      },
      generateToken: function(testId, ttl) {
        var tokenTTL = moment().add(ttl, 'minutes').unix();

        var token = crypto.createHash('md5').update(tokenTTL.toString()).digest('hex');

        return token;
      }
    },
    associate: function(models) {
      Writing_test.belongsTo(models.trainer);
      Writing_test.belongsTo(models.client);
      Writing_test.belongsTo(models.company);
      Writing_test.belongsTo(models.language);
      Writing_test.hasOne(models.writing_test_result);
      Writing_test.belongsTo(models.candidate);
      Writing_test.belongsTo(models.writing_test_pattern);
    }
  });
  return Writing_test;
};
