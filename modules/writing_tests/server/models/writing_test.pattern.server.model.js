"use strict";

module.exports = function(sequelize, DataTypes) {

  var Writing_testPattern = sequelize.define('writing_test_pattern', {
    name: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: true
      }
    },
    questions: {
      type: DataTypes.TEXT,
      set: function(val) {
        this.setDataValue('questions', JSON.stringify(val));
      },
      get: function() {
        return JSON.parse(this.getDataValue('questions'));
      }
    },
  }, {
    associate: function(models) {
      Writing_testPattern.belongsTo(models.language);
      Writing_testPattern.belongsToMany(models.company,{through: 'writing_test_pattern_company'});
      Writing_testPattern.hasMany(models.writing_test);
    }
  });
  return Writing_testPattern;
};