"use strict";

module.exports = function(sequelize, DataTypes) {

  var Writing_testResult = sequelize.define('writing_test_result', {
    answers: {
      type: DataTypes.TEXT,
      validate: {
        notEmpty: true
      },
      set: function(val) {
        this.setDataValue('answers', JSON.stringify(val));
      },
      get: function() {
        return JSON.parse(this.getDataValue('answers'));
      }
    },
    countAllAnswers: {
      type: DataTypes.INTEGER
    },
    countAllRadio: {
      type: DataTypes.INTEGER
    },
    countCorrectRadio: {
      type: DataTypes.INTEGER
    },
    countAllText: {
      type: DataTypes.INTEGER
    }
  }, {
    associate: function(models) {
      Writing_testResult.belongsTo(models.writing_test);
    }
  });
  return Writing_testResult;
};