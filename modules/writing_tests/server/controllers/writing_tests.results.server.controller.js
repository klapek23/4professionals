'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash'),
  fs = require('fs'),
  async = require('async'),
  path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')).models,
  multer = require('multer'),
  clone = require('clone'),
  moment = require('moment'),
  config = require(path.resolve('./config/config')),
  nodemailer = require('nodemailer'),
  smtpTransport = nodemailer.createTransport(config.mailer.options),
  fs = require('fs');

/**
 * Create new result
 */
var createTestResult = exports.create = function(req, res) {
  var result = req.body;
      
  db.writing_test.findOne({
    where: {
      id: req.params.writing_testId
    },
    include: [
      {model: db.writing_test_pattern}
    ]
  }).then(function(test) {
    if(moment(test.activeTo).format('YYYY-MM-DD HH:mm') < moment().format('YYYY-MM-DD HH:mm')) {
      res.status(400).send({
        message: 'Test is not active, session time is over'
      });
    }

    var calculatedResult = calculateResult(result, test);
    
    return db.writing_test_result.create(calculatedResult).then(function(result) {
      return test.updateAttributes({
        status: calculatedResult.status
      }).then(function(test) {
        return result.setWriting_test(test).then(function() {
          //res.send('ok');
          res.json({
            message: 'ok'
          });
        }, function(err) {
          res.status(400).send({
            message: 'Error with create relation'
          });
        });
      }, function(err) {
        res.status(400).send({
          message: 'Error with updating test status'
        });
      });
    }, function(err) {
      res.status(400).send({
        message: 'Error with saving test result'
      });
    });
  });
};


/**
 * Update result
 */
var updateTestResult = exports.update = function(req, res) {
  db.writing_test_result.findOne({
    where: {
      writingTestId: req.params.writing_testId
    },
    include: [
      { model: db.writing_test, include: [db.language, db.writing_test_pattern, db.candidate,
          { model: db.client, include: [db.user, db.company] },
          { model: db.trainer, include: [db.user] }
        ] }
    ]
  }).then(function(testResult) {
    var test = testResult.writing_test;

    if(moment(test.activeTo).format('YYYY-MM-DD HH:mm') < moment().format('YYYY-MM-DD HH:mm')) {
      res.status(400).send({
        message: 'Test is not active, session time is over'
      });
    }

    var calculatedResult = calculateResult(req.body, test);
    testResult.updateAttributes({
      answers: calculatedResult.answers,
      countCorrectRadio: calculatedResult.countCorrectRadio,
      countAllRadio: calculatedResult.countAllRadio,
      countAllText: calculatedResult.countAllText,
      countAllAnswers: calculatedResult.countAllAnswers
    }).then(function(testResult) {
      //send message info
    /*res.render(path.resolve('modules/writing_tests/server/templates/finish-writing-test-confirmation-email'), {
      appName: config.app.title,
      client: (test.send_to_company && test.client.company.email ? 'Sir / Madam' : test.client.user.username),
      company: test.client.company.name,
      candidateFullName: test.candidate.name,
      candidatePhone: test.candidate.phone,
      language: test.language.title,
      notes: test.notes,
      expiresAt: moment(test.expiresAt).format('YYYY-MM-DD HH:mm'),
      url: config.app.mainUrl,
      testUrl: config.app.mainUrl + '/writing_tests/' + test.id
      }, function(err, emailHTML) {
        if(err) {
          res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          var mailOptions = {
            to: (test.send_to_company && test.client.company.email ? test.client.company.email : test.client.user.email),
            from: config.mailer.from,
            subject: 'The trainer rated writing test',
            html: emailHTML
          };
          smtpTransport.sendMail(mailOptions, function(err) {
            if (!err) {
              res.json(testResult);
            } else {
              return res.status(400).send({
                message: 'Failure sending email to client'
              });
            }
          });
        }
      });*/

      res.json({
        message: 'ok'
      });
      //res.json(testResult);
    }, function(err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
  }, function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Create or update
 */
exports.createOrUpdate = function(req, res) {
  db.writing_test.findOne({
    where: {
      id: req.params.writing_testId
    },
    include: [
      { model: db.writing_test_result }
    ]
  }).then(function(test) {
    if(test.writing_test_result) {
      return updateTestResult(req, res);
    } else {
      return createTestResult(req, res);
    }
  });
};

/**
 * Delete result
 */
exports.delete = function(req, res) {
  
};


/**
 * Find one result
 * 
 * @param {obj} req
 * @param {obj} res
 * @returns {json}
 */
exports.findOne = function(req, res) {
  db.writing_test_result.findOne({
    where: {
      writingTestId: req.params.writing_testId
    }
  }).then(function(testResult) {
    res.json(testResult);
  }, function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Download writing test result
 */
exports.downloadResult = function(req, res) {
  db.writing_test_result.findOne({
    where: {
      writingTestId: req.params.testId
    }
  }).then(function(testResult) {
    var file = './public/uploads/writing_tests/results/' + testResult.result_file;
    res.download(file); // Set disposition and send it.
  }, function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 Calculate results
 */
function calculateResult(result, test) {
  var calculatedResult = Object.assign({}, result, {
    countCorrectRadio: 0,
    countAllRadio: 0,
    countAllText: 0,
    countAllAnswers: 0
  });

  for(var answerKey in calculatedResult.answers){
    calculatedResult.countAllAnswers++;

    var questionId = parseInt(answerKey.replace('q_', ''), 10);
    if(typeof calculatedResult.answers[answerKey] === 'object') {
      calculatedResult.countAllRadio++;

      var answerId = parseInt(calculatedResult.answers[answerKey].id.replace('answer_', ''), 10);
      calculatedResult.answers[answerKey].id = 'answer_' + questionId;

      //check if answer is correct
      if(answerId === test.writing_test_pattern.questions[questionId].correct) {
        calculatedResult.answers[answerKey].isCorrect = true;
        calculatedResult.countCorrectRadio++;
      } else {
        calculatedResult.answers[answerKey].isCorrect = false;
      }
    } else {
      var val = calculatedResult.answers[answerKey];

      calculatedResult.answers[answerKey] = {};
      calculatedResult.answers[answerKey].id = 'answer_' + questionId;
      calculatedResult.answers[answerKey].val = val;
      calculatedResult.countAllText++;
    }
  }

  return calculatedResult;
}
