'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  logger = require('../../../../config/lib/winston'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  Pattern = db.writing_test_pattern,
  Language = db.language,
  Company = db.company,
  config = require(path.resolve('./config/config')),
  moment = require('moment'),
  nodemailer = require('nodemailer'),
  smtpTransport = nodemailer.createTransport(config.mailer.options),
  async = require('async');
  

/**
 * Create a pattern
 */
exports.create = function(req, res) {
  async.waterfall([
    function(callback) {
      Pattern.create({
        name: req.body.name,
        questions: req.body.questions
      }).then(function(pattern) {
        callback(null, pattern);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(pattern, callback) {
      Language.findById(req.body.language.id).then(function(language) {
        callback(null, pattern, language);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(pattern, language, callback) {
      pattern.setLanguage(language).then(function() {
        pattern.language = language;
        callback(null, pattern);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(pattern, callback) {
      pattern.companies = [];
      if(typeof req.body.companies !== 'undefined') {
        req.body.companies.forEach(function(el, i) {
          pattern.companies[i] = db.company.build(el);
        });
        
        pattern.setCompanies(pattern.companies).then(function() {
          callback(null, pattern);
        }).catch(function(err) {
          callback(err);
        });
      } else {
        callback(null, pattern);
      }
    }
  ], function(err, pattern) {
    if(err) {
      return res.status(400).send({
        message: err.message
      });
    } else {
      return res.json(pattern);
    }
  });
};


/**
 * Update a test pattern
 */
exports.update = function(req, res) {
  var pattern = req.pattern;

  async.waterfall([
    function(callback) {
      pattern.updateAttributes({
        name: req.body.name,
        questions: req.body.questions
      }).then(function(pattern) {
        callback(null, pattern);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(pattern, callback) {
      Language.findOne({
        where: {
          id: req.body.language.id
        }
      }).then(function(language) {
        if(language) {
          callback(null, pattern, language);
        } else {
          callback(new Error('No laguage found'), pattern, language);
        }
      }).catch(function(err) {
        callback(err);
      });
    },
    function(pattern, language, callback) {
      pattern.setLanguage(language).then(function(pattern) {
        callback(null, pattern, language);
      }).catch(function(err) {
        callback(err);
      });
    },
    function(pattern, language, callback) {
      pattern.companies = [];
      if(typeof req.body.companies !== 'undefined') {
        req.body.companies.forEach(function(el, i) {
          pattern.companies[i] = db.company.build(el);
        });

        pattern.setCompanies(pattern.companies).then(function() {
          callback(null, pattern);
        }).catch(function(err) {
          callback(err);
        });
      } else {
        callback(null, pattern);
      }
    }
  ], function(err, pattern) {
    if(err) {
      return res.status(400).send({
        message: err.message
      });
    } else {
      return res.json(pattern);
    }
  });
};


/**
 * Delete a pattern
 */
exports.delete = function(req, res) {
   var pattern = req.pattern;

  // Find the pattern
  Pattern.findById(pattern.id).then(function(pattern) {
    if (pattern) {

      // Delete the pattern
      return pattern.destroy().then(function() {
        return res.json(pattern);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find the article'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Get single test by ID
 */
exports.getByID = function(req, res) {
  Pattern.findOne({
    where: {
      id: req.params.patternId
    },
    include: [
      { model: db.language },
      { model: db.company }
    ]
  }).then(function(pattern) {
    res.json(pattern);
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * List tests
 */
exports.list = function(req, res) {
  Pattern.findAll({
    include: [
      db.language
    ]
  }).then(function(patterns) {
    if (!patterns) {
      return res.status(404).send({
        message: 'No patterns found'
      });
    } else {
//      patterns.forEach(function(pattern, i) {
//        pattern.getCompanies();
//      });
      
      res.json(patterns);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Get by language
 */
exports.getByLanguage = function(req, res) {
  Pattern.findAll({
    where: {
      languageId: req.params.languageId
    },
    include: [
      { model: db.language }
    ]
  }).then(function(patterns) {
    if (!patterns) {
      return res.status(404).send({
        message: 'No patterns found'
      });
    } else {
      res.json(patterns);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Get by lang and company
 */
exports.getByLanguageAndCompany = function(req, res) {
  db.company.findById(req.params.companyId).then(function(company) {
    return company.getWriting_test_patterns({
      where: {
        languageId: req.params.languageId
      }
    }).then(function(patterns) {
      if (!patterns) {
        return res.status(404).send({
          message: 'No patterns found'
        });
      } else {
        res.json(patterns);
      }
    }).catch(function(err) {
      console.log(err);
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Writing test pattern middleware
 */
exports.getPatternID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Writing test pattern is invalid'
    });
  }

  Pattern.find({
    where: {
      id: id
    },
    include: [
      { model: db.language }
    ]
  }).then(function(pattern) {
    if (!pattern) {
      return res.status(404).send({
        message: 'No writing test pattern with that identifier has been found'
      });
    } else {
      req.pattern = pattern;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};