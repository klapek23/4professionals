'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  db = require(path.resolve('./config/lib/sequelize')).models,
  Test = db.writing_test,
  Candidate = db.candidate,
  config = require(path.resolve('./config/config')),
  moment = require('moment'),
  nodemailer = require('nodemailer'),
  generator = require('generate-password'),
  smtpTransport = nodemailer.createTransport(config.mailer.options),
  crypto = require('crypto'),
  jwt = require('jsonwebtoken'),
  async = require('async'),
  fs = require('fs');
  

/**
 * Create a test
 */
exports.create = function(req, res) {
  Test.create(req.body).then(function(test) {
    if (!test) {
      return res.status(400).send({
        message: 'Unable to create new writing test'
      });
    } else {
      return Candidate.create({
        name: req.body.candidateFullName,
        email: req.body.candidateEmail
      }).then(function(candidate) {
        return test.setCandidate(candidate).then(function() {
          //generate TOKEN
          if(req.body.ttl.expires) {
            test.token = jwt.sign({ 
              testId: test.id, 
              ignoreExpiration: false 
            }, config.app.secret, { expiresIn: req.body.ttl.expires });
          } else {
            test.token = jwt.sign({ 
              testId: test.id, 
              ignoreExpiration: true 
            }, config.app.secret);
          }

          //generate password
          test.salt = test.makeSalt();
          var password = generator.generate({
                        length: 10,
                        numbers: true,
                        symbols: true
                      });

          test.password = test.encryptPassword(password, test.salt);

          return test.save().then(function(test) {
            var language = test.getLanguage();

            //send message info
            res.render(path.resolve('modules/writing_tests/server/templates/create-writing-test-confirmation-email'), {
              appName: config.app.title,
              candidateFullName: candidate.name,
              //candidatePhone: candidate.phone,
              language: language.title,
              token: test.token,
              expiresAt: moment(test.expiresAt).format('YYYY-MM-DD HH:mm'),
              notes: test.notes,
              url: config.app.mainUrl
            }, function(err, emailHTML) {
              if(err) {
                res.status(400).send({
                  message: errorHandler.getErrorMessage(err)
                });
              } else {
                //mail to candidate about new test
                var mailOptions = {
                  to: candidate.email,
                  from: config.mailer.from,
                  subject: 'Language writing test',
                  html: emailHTML
                };
                smtpTransport.sendMail(mailOptions, function(err) {
                  if (!err) {
                    res.json(test);
                  } else {
                    return res.status(400).send({
                      message: 'Failure sending email to candidate'
                    });
                  }
                });
              }
            });
          });
        });
      });
    }
  }, function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Update a test
 */
exports.update = function(req, res) {
  var test = req.test;

  async.waterfall([
    function(callback) {
      test.updateAttributes({
        notes: req.body.notes,
        status: (req.body.hasOwnProperty('status') ? req.body.status : null),
        send_to_company: req.body.send_to_company,
        languageId: req.body.language.id,
        trainerId: (req.body.hasOwnProperty('trainer') && req.body.trainer !== null ? req.body.trainer.id : null),
        clientId: req.body.clientId,
        companyId: req.body.companyId,
        writingTestPatternId: req.body.writing_test_pattern.id
      }).then(function(test) {
        callback(null, test);
      }, function(err) {
        callback(err);
      });
    },
    function(test, callback) {
      Candidate.findOne({
        where: {
          name: test.candidate.id
        }
      }).then(function(candidate) {
        callback(null, test, candidate);
      }, function(err) {
        callback(err);
      });
    },
    function(test, candidate, callback) {
      if(candidate) {
        candidate.updateAttributes({
          name: req.body.candidateFullName,
          phone: req.body.candidatePhone,
          email: req.body.candidateEmail
        }).then(function(candidate) {
          callback(null, test, candidate);
        }, function(err) {
          callback(err);
        });
      } else {
        Candidate.create({
          name: req.body.candidateFullName,
          email: req.body.candidateEmail
        }).then(function(candidate) {
          callback(null, test, candidate);
        }, function(err) {
          callback(err);
        });
      }
    },
    function(test, candidate, callback) {
      test.setCandidate(candidate).then(function() {
        callback(null, test, candidate);
      }, function(err) {
        callback(err);
      });
    }
  ], function(err, test, candidate) {
    if(err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      return res.json(test);
    }
  });
};


/**
 * 
 * @param {obj} req
 * @param {obj} res
 * @returns {obj}
 */
exports.updateStatus = function(req, res) {
  
  async.waterfall([
    function(callback) {
      Test.findOne({
        where: {
          id: req.test.id
        },
        include: [
          { model: db.trainer, include: [ db.user ] },
          { model: db.client, include: [ db.user, db.company ] },
          db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
        ]
      }).then(function(test) {
        callback(null, test);
      }, function(err) {
        callback(err);
      });
    },
    function(test, callback) {
      test.updateAttributes({
        status: req.body.status,
        level: req.body.level,
        notes: req.body.notes
      }).then(function(test) {
        callback(null, test);
      }, function(err) {
        callback(err);
      });
    },
    function(test, callback) {
      if((req.body.hasOwnProperty('trainer') && req.body.trainer !== null) || (req.body.hasOwnProperty('trainerId') && req.body.trainerId !== null)) {
        db.trainer.findOne({
          where: {
            id: req.body.trainerId
          },
          include: [db.user]
        }).then(function(trainer) {
          callback(null, test, trainer);
        }, function(err) {
          callback(err);
        });
      } else {
        callback(null, test, null);
      }
    },
    function(test, trainer, callback) {
      if(trainer !== null) {
        test.setTrainer(trainer);
        test.trainer = trainer;
        callback(null, test);
        
      } else {
        callback(null, test);
      }
    },
    function(test, callback) {
      if(test.status === 'filled') {
        res.render(path.resolve('modules/writing_tests/server/templates/fill-writing-test-confirmation-email'), {
          appName: config.app.title,
          client: (test.send_to_company && test.client.company.email ? 'Sir / Madam' : test.client.user.username),
          candidateFullName: test.candidate.name,
          candidatePhone: test.candidate.phone,
          language: test.language.title,
          notes: test.notes,
          url: config.app.mainUrl,
          testUrl: config.app.mainUrl + '/writing_tests/' + test.id
        }, function(err, emailHTML) {
          if(err) {
            callback(err);
          } else {
            var mailOptions = {
              to: (test.send_to_company && test.client.company.email ? test.client.company.email : test.client.user.email),
              from: config.mailer.from,
              subject: 'The writing test has been completed by the candidate',
              html: emailHTML
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              if (!err) {
                callback(null, test);
              } else {
                callback(err);
              }
            });
          }
        });
      } else if (test.status === 'waiting for trainer') {
        res.render(path.resolve('modules/writing_tests/server/templates/set-trainer-writing-test-confirmation-email'), {
          appName: config.app.title,
          trainer: test.trainer.user.displayName,
          client: test.client.user.username,
          candidateFullName: test.candidate.name,
          candidatePhone: test.candidate.phone,
          language: test.language.title,
          notes: test.notes,
          expiresAt: moment(test.expiresAt).format('YYYY-MM-DD HH:mm'),
          url: config.app.mainUrl,
          testUrl: config.app.mainUrl + '/writing_tests/' + test.id
        }, function(err, emailHTML) {
          if(err) {
            callback(err);
          } else {
            var mailOptions = {
              to: test.trainer.user.email,
              from: config.mailer.from,
              subject: 'You got new writing assessment order',
              html: emailHTML
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              if (!err) {
                callback(null, test);
              } else {
                callback(err);
              }
            });
          }
        });
      /*} else if (test.status === 'canceled' && (test.candidate.email && test.candidate.email !== null)) {
        res.render(path.resolve('modules/writing_tests/server/templates/cancel-writing-test-confirmation-email'), {
          appName: config.app.title,
          company: test.client.company.name,
          candidateFullName: test.candidate.name,
          candidatePhone: test.candidate.phone,
          language: test.language.title,
          notes: test.notes,
          expiresAt: moment(test.expiresAt).format('YYYY-MM-DD HH:mm'),
          url: config.app.mainUrl,
          testUrl: config.app.mainUrl + '/writing_tests/' + test.id
        }, function(err, emailHTML) {
          if(err) {
            callback(err);
          } else {
            var mailOptions = {
              to: test.candidate.email,
              from: config.mailer.from,
              subject: 'Writing test has been canceled',
              html: emailHTML
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              if (!err) {
                callback(null, test);
              } else {
                callback(err);
              }
            });
          }
        });*/
      } else if(test.status === 'done') {
        res.render(path.resolve('modules/writing_tests/server/templates/finish-writing-test-confirmation-email'), {
          appName: config.app.title,
          client: (test.send_to_company && test.client.company.email ? 'Sir / Madam' : test.client.user.username),
          company: test.client.company.name,
          candidateFullName: test.candidate.name,
          candidatePhone: test.candidate.phone,
          language: test.language.title,
          notes: test.notes,
          expiresAt: moment(test.expiresAt).format('YYYY-MM-DD HH:mm'),
          url: config.app.mainUrl,
          testUrl: config.app.mainUrl + '/writing_tests/' + test.id
      }, function(err, emailHTML) {
        if(err) {
          res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          var mailOptions = {
            to: (test.send_to_company && test.client.company.email ? test.client.company.email : test.client.user.email),
            from: config.mailer.from,
            subject: 'The writing test has been assessed by the trainer',
            html: emailHTML
          };
          smtpTransport.sendMail(mailOptions, function(err) {
            if (!err) {
              res.json(test);
            } else {
              return res.status(400).send({
                message: 'Failure sending email to client'
              });
            }
          });
        }
      });
      } else {
        callback(null, test);
      }
    }
  ], function(err, test) {
    if(err) {
      return res.status(400).send({
        message: err.message
      });
    } else {
      return res.json(test);
    }
  });
};


/**
 * Delete a test
 */
exports.delete = function(req, res) {
  var test = req.test;

  // Find the test
  Test.findById(test.id).then(function(test) {
    if (test) {

      // Delete the test
      return test.destroy().then(function() {
        return res.json(test);
      }).catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

    } else {
      return res.status(400).send({
        message: 'Unable to find test'
      });
    }
  }).catch(function(err) {
    return res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Get single test by ID
 */
exports.findOne = function(req, res) {
  db.writing_test.findOne({
    where: {
      id: req.params.writing_testId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
    ]
  }).then(function(test) {
    if (!test) {
      return res.status(404).send({
        message: 'Test not found'
      });
    } else {
      res.json(test);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * 
 * Get by token
 */
exports.getByToken = function(req, res) {
  
  try {
    var ignoreExpiration = ((jwt.decode(req.params.token) && jwt.decode(req.params.token).ignoreExpiration) ? jwt.decode(req.params.token).ignoreExpiration : false);
  } catch(err) {
    return res.status(400).send({
      message: 'Error: Token not correct'
    });
  }

  jwt.verify(req.params.token, config.app.secret, function(err, decoded) {
    if(err) {
      var message;
      
      switch(err.message) {
        case 'jwt expired':
          message = 'Token expired';
          break;
        case 'jwt malformed':
          message = 'Token not correct';
          break;
        default:
          message = err.message;
      }
      return res.status(400).send({
        message: 'Error: ' + message
      });
    } else {
      var testData = {};
      async.waterfall([
        function(callback) {
          Test.findOne({
            where: {
              id: decoded.testId
            },
            include: [
              { model: db.trainer, include: [ db.user ] },
              { model: db.client, include: [ db.user, db.company ] },
              db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
            ]
          }).then(function(test) {
            if (!test) {
              callback(new Error('Test not found'));
            } else {
              if(test.status !== 'pending') {
                callback(new Error('The link is not active. You have already taken this test.'));
              } else if(test.startedAt && moment(test.activeTo).format('YYYY-MM-DD HH:mm') < moment().format('YYYY-MM-DD HH:mm')) {
                callback(new Error('The link is not active. Session time is over.'));
              } else {
                callback(null, test);
              }
            }
          }).catch(function(err) {
            callback(err);
          });
        },
        function(test, callback) {
          testData.testId = test.id;

          testData.candidate = {
            name: test.candidate.name,
            email: test.candidate.email
          };

          testData.createdAt = test.createdAt;
          testData.expiresAt = test.expiresAt;
          testData.language = test.language.title;
          testData.questions = test.writing_test_pattern.questions;

          testData.questions.forEach(function(q, i) {
            if(q.type === 'radio' && testData.questions[i].correct !== 'undefined') {
              delete testData.questions[i].correct;
            }
          });

          if(!test.startedAt || !test.activeTo) {
            test.updateAttributes({
              startedAt: moment().format('YYYY-MM-DD HH:mm'),
              activeTo: moment().add(1, 'h').format('YYYY-MM-DD HH:mm')
            }).then(function(test) {
              testData.startedAt = test.startedAt;
              testData.activeTo = test.activeTo;

              callback(null, test);
            }).catch(function(err) {
              callback(err);
            });
          } else {
            testData.startedAt = test.startedAt;
            testData.activeTo = test.activeTo;

            callback(null, test);
          }
        }
      ], function(err, test) {
        if(err) {
          return res.status(400).send({
            message: err.message
          });
        } else {
          return res.json(testData);
        }
      });
    }
  });
};


/**
 * List tests
 */
exports.list = function(req, res) {
  Test.findAll({
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
    ]
  }).then(function(tests) {
    if (!tests) {
      return res.status(404).send({
        message: 'No tests found'
      });
    } else {
      res.json(tests);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * List tests by company
 */
exports.getByCompany = function(req, res) {
  Test.findAll({
    where: {
      companyId: req.params.companyId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
    ]
  }).then(function(tests) {
    if (!tests) {
      return res.status(404).send({
        message: 'No tests found'
      });
    } else {
      res.json(tests);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * List tests by client
 */
exports.getByClient = function(req, res) {
  Test.findAll({
    where: {
      clientId: req.params.clientId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
    ]
  }).then(function(tests) {
    if (!tests) {
      return res.status(404).send({
        message: 'No tests found'
      });
    } else {
      res.json(tests);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * List tests by trainer
 */
exports.getByTrainer = function(req, res) {
  Test.findAll({
    where: {
      trainerId: req.params.trainerId
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
    ]
  }).then(function(tests) {
    if (!tests) {
      return res.status(404).send({
        message: 'No tests found'
      });
    } else {
      res.json(tests);
    }
  }, function(err) {
    res.status(400).send({
      message: errorHandler.getErrorMessage(err)
    });
  });
};


/**
 * Writing test pattern middleware
 */
exports.getTestID = function(req, res, next, id) {

  if ((id % 1 === 0) === false) { //check if it's integer
    return res.status(404).send({
      message: 'Writing test id is invalid'
    });
  }

  Test.findOne({
    where: {
      id: id
    },
    include: [
      { model: db.trainer, include: [ db.user ] },
      { model: db.client, include: [ db.user, db.company ] },
      db.language, db.writing_test_result, db.writing_test_pattern, db.candidate
    ]
  }).then(function(test) {
    if (!test) {
      return res.status(404).send({
        message: 'No writing test with that identifier has been found'
      });
    } else {
      req.test = test;
      next();
    }
  }).catch(function(err) {
    return next(err);
  });

};
