'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  writing_testsPolicy = require('../policies/writing_tests.server.policy'),
  writing_testsFillPolicy = require('../policies/writing_tests.fill.server.policy'),
  writing_testsResultsPolicy = require('../policies/writing_tests.results.server.policy'),
  writing_testsResults = require(path.resolve('./modules/writing_tests/server/controllers/writing_tests.results.server.controller')),
  writing_testsPatternsPolicy = require('../policies/writing_tests.patterns.server.policy'),
  writing_testsPatterns = require(path.resolve('./modules/writing_tests/server/controllers/writing_tests.patterns.server.controller')),
  writing_tests = require(path.resolve('./modules/writing_tests/server/controllers/writing_tests.server.controller')),
  multer = require('multer'),
  upload = multer({ dest: 'public/uploads/writing_tests/results/' });


module.exports = function(app) {
  
  //display writing test for candidate
  app.route('/api/writing_tests/get/by-token/:token')
    .get(/*writing_testsFillPolicy.isAllowed, */writing_tests.getByToken);
  
  // Writing tests patterns collection routes
  app.route('/api/writing_tests/patterns')
    .all(writing_testsPatternsPolicy.isAllowed)
    .get(writing_testsPatterns.list)
    .post(writing_testsPatterns.create);
    
  app.route('/api/writing_tests/patterns/:patternId')
    .all(writing_testsPatternsPolicy.isAllowed)
    .get(writing_testsPatterns.getByID)
    .put(writing_testsPatterns.update)
    .delete(writing_testsPatterns.delete);

  // Writing tests collection routes
  app.route('/api/writing_tests')
    .all(writing_testsPolicy.isAllowed)
    .get(writing_tests.list)
    .post(writing_tests.create);
    
  app.route('/api/writing_tests/:writing_testId')
    .all(writing_testsPolicy.isAllowed)
    .get(writing_tests.findOne)
    .put(writing_tests.update)
    .delete(writing_tests.delete);
    
  app.route('/api/writing_tests/update/by-id/:writing_testId')
    .put(writing_tests.updateStatus);
    
  app.route('/api/writing_tests/get/by-trainer/:trainerId')
    .get(writing_testsPolicy.isAllowed, writing_tests.getByTrainer);
    
  app.route('/api/writing_tests/get/by-client/:clientId')
    .get(writing_testsPolicy.isAllowed, writing_tests.getByClient);
  
  app.route('/api/writing_tests/get/by-company/:companyId')
    .get(writing_testsPolicy.isAllowed, writing_tests.getByCompany);
    
  app.route('/api/writing_tests/:writing_testId/result')
    .all(writing_testsResultsPolicy.isAllowed)
    .get(writing_testsResults.findOne)
    .post(writing_testsResults.createOrUpdate)
    .put(writing_testsResults.update);
    
  app.route('/api/writing_tests/patterns/get/by-language/:languageId/company/:companyId')
    .get(writing_testsPatternsPolicy.isAllowed, writing_testsPatterns.getByLanguageAndCompany);
    
  app.route('/api/writing_tests/patterns/get/by-language/:languageId')
    .get(writing_testsPatternsPolicy.isAllowed, writing_testsPatterns.getByLanguage);

  // Finish by binding the middleware
  app.param('writing_testId', writing_tests.getTestID);
  app.param('patternId', writing_testsPatterns.getPatternID);
};
