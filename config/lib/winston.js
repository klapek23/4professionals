"use strict";

/**
 * Created by Junaid Anwar on 5/28/15.
 */
var winston = require('winston');
var logger = new(winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      level: 'verbose',
      prettyPrint: true,
      colorize: true,
      silent: false,
      timestamp: false
    }),
    new (winston.transports.File)({
      level: 'error',
      name: 'error-file',
      filename: __dirname + '/../../error-logs.log',
      handleExceptions: true,
      json: true,
      maxFiles: 15,
      colorize: false
    }),
    new (winston.transports.File)({
      level: 'info',
      name: 'info-file',
      filename: __dirname + '/../../logs/info-logs.log',
      json: true,
      maxFiles: 15,
      colorize: false
    })
  ]
});


logger.stream = {
  write: function(message, encoding) {
    logger.info(message);
  }
};

module.exports = logger;