'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.min.css',
        'public/lib/bootstrap/dist/css/bootstrap-theme.min.css',
        'public/lib/angular-bootstrap/ui-bootstrap-csp.css',
        'public/lib/angular-notify/dist/angular-notify.min.css',
        'public/lib/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css',
        'public/lib/csspin/csspin.css',
        'modules/core/client/less/fonts/font-awesome.min.css'
      ],
      js: [
        'public/lib/interact/dist/interact.min.js',
        'public/lib/moment/min/moment-with-locales.min.js',
        'public/lib/moment-timezone/moment-timezone.js',
        'public/lib/moment-round/dist/moment-round.min.js',
        'public/lib/moment-range/dist/moment-range.min.js',
        'public/lib/angular/angular.min.js',
        'public/lib/angular-resource/angular-resource.min.js',
        'public/lib/angular-cookies/angular-cookies.min.js',
        'public/lib/angular-animate/angular-animate.min.js',
        'public/lib/angular-messages/angular-messages.min.js',
        'public/lib/angular-ui-router/release/angular-ui-router.min.js',
        'public/lib/angular-ui-utils/ui-utils.min.js',
        'public/lib/angular-bootstrap/ui-bootstrap.min.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'public/lib/angular-file-upload/angular-file-upload.min.js',
        'public/lib/angular-notify/dist/angular-notify.min.js',
        'public/lib/angular-bootstrap-multiselect/dist/angular-bootstrap-multiselect.min.js',
        'public/lib/angular-bootstrap-multiselect/dist/angular-bootstrap-multiselect-templates.min.js',
        'public/lib/angular-touch/angular-touch.min.js',
        'public/lib/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar.min.js',
        'public/lib/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js',
        'public/lib/angular-tablesort/js/angular-tablesort.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
        'https://www.google.com/recaptcha/api.js'
      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    'main_css_dir': [
      'modules/core/client/css/'
    ],
    css: [
      'modules/*/client/css/*.css'
    ],
    'main_less_dir': [
      'modules/core/client/less/'
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: 'gruntfile.js',
    gulpConfig: 'gulpfile.js',
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: 'modules/*/server/config/*.js',
    policies: 'modules/*/server/policies/*.js',
    views: 'modules/*/server/views/*.html'
  }
};
