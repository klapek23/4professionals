'use strict';

module.exports = {
  app: {
    title: '4professionals - Accent for professionals',
    description: 'Full-Stack Javascript with SequelizeJS, ExpressJS, AngularJS, and NodeJS',
    keywords: 'SequelizeJS, ExpressJS, AngularJS, NodeJS',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID',
    supportEmail: 'language.assessments@4professionals.pl',
    secret: 'dI61QZD4jkPk0ydExFKyw7vhj90t50Dw'
  },
  port: process.env.PORT || 3000,
  templateEngine: 'swig',
  // Session Cookie settings
  sessionCookie: {
    // session expiration is set by default to 24 hours
    //maxAge: 24 * (60 * 60 * 1000),
    maxAge: 60 * 60 * 1000,
    // httpOnly flag makes sure the cookie is only accessed
    // through the HTTP protocol and not JS/browser
    httpOnly: true,
    // secure cookie should be turned to true to provide additional
    // layer of security so that the cookie is set only when working
    // in HTTPS mode.
    secure: Boolean(process.env.ssl) || true
  },
  // sessionSecret should be changed for security measures and concerns
  sessionSecret: 'i21CG1KvS6y460zcczvqa7AY2Yfu29b5',
  // sessionKey is set to the generic sessionId key used by PHP applications
  // for obsecurity reasons
  sessionKey: 'sessionId',
  sessionCollection: 'sessions',
  logo: 'modules/core/client/img/brand/logo.png',
  favicon: 'modules/core/client/img/brand/favicon.ico',
  admin: {
    name: 'klapek23',
    fullName: 'Dawid Wojtyca',
    email: 'klapek23@gmail.com'
  },
  timezone: 'Europe/Warsaw',
};
