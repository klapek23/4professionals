'use strict';

var defaultEnvConfig = require('./default');

module.exports = {
  db: {
    name: process.env.DB_NAME || "4professionals_05-11-2015",
    host: process.env.DB_HOST || "localhost",
    port: process.env.DB_PORT || 3306,
    username: process.env.DB_USERNAME || "root",
    password: process.env.DB_PASSWORD || "",
    dialect: process.env.DB_DIALECT || "mysql",
    enableSequelizeLog: process.env.DB_LOG || false,
    ssl: process.env.DB_SSL || false,
    sync: process.env.DB_SYNC || true //Synchronizing any model changes with database
  },
  redis: {
    host: process.env.REDIS_HOST || "localhost",
    port: process.env.REDIS_PORT || 6379,
    database: process.env.REDIS_DATABASE || 0,
    password: process.env.REDIS_PASSWORD || "",
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      //stream: 'access.log'
    }
  },
  app: {
    title: defaultEnvConfig.app.title + ' - Development Environment',
    mainUrl: 'http://localhost:3000',
    supportEmail: 'klapek23@gmail.com'
  },
  mailer: {
    from: process.env.MAILER_FROM || 'klapek23@gmail.com',
    options: {
      service: process.env.MAILER_SERVICE_PROVIDER || 'gmail',
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'klapek23@gmail.com',
        pass: process.env.MAILER_PASSWORD || 'n2&U4d5XMk'
      }
    }
  },
  sessionCookie: {
    // session expiration is set by default to 24 hours
    //maxAge: 24 * (60 * 60 * 1000),
    httpOnly: false,
    maxAge: 2 * (60 * 60 * 1000),
    secure: false
  },
  timezone: 'Europe/Warsaw',
  livereload: true
};
