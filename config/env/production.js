'use strict';

var defaultEnvConfig = require('./default');

module.exports = {
  secure: {
    ssl: Boolean(process.env.ssl) || false,
    privateKey: './config/sslcerts/key.pem',
    certificate: './config/sslcerts/cert.pem'
  },
  port: process.env.PORT || 3000,
  db: {
    name: process.env.DB_NAME || "4professionals", //"seanjs_dev",
    host: process.env.DB_HOST || "localhost",
    port: process.env.DB_PORT || 3306,
    username: process.env.DB_USERNAME || "root",
    password: process.env.DB_PASSWORD || "vrlaMGIri4", //"",
    dialect: process.env.DB_DIALECT || "mysql", //mysql, postgres, sqlite3,...
    enableSequelizeLog: process.env.DB_LOG || false,
    ssl: process.env.DB_SSL || false,
    sync: process.env.DB_SYNC || false //Synchronizing any model changes with database
  },
  redis: {
    host: process.env.REDIS_HOST || "localhost",
    port: process.env.REDIS_PORT || 6379,
    database: parseInt(process.env.REDIS_DATABASE) || 0,
    password: process.env.REDIS_PASSWORD || "",
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'combined',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      stream: 'access.log'
    }
  },
  mailer: {
    from: process.env.MAILER_FROM || 'latool@4professionals.pl',
    options: {
      //service: process.env.MAILER_SERVICE_PROVIDER || 'mail.4professionals.pl',
      host: process.env.MAILER_SERVICE_PROVIDER || 'mail.4professionals.pl',
      //name: process.env.MAILER_SERVICE_PROVIDER || 'mail.4professionals.pl',
      port: 587,
      debug: true,
      logger: true,
      tls: {
        rejectUnauthorized: false
      },
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'latool@4professionals.pl',
        pass: process.env.MAILER_PASSWORD || 'gzwZjGS6'
      }
    }
  },
  app: {
    title: 'LA-Tool',
    mainUrl: 'http://latool.pl:3000',
    supportEmail: 'language.assessments@4professionals.pl'
  },
  admin: {
    name: 'LA-Tool',
    fullName: 'Administrator',
    email: 'latool@4professionals.pl'
  },
  sessionCookie: {
    // session expiration is set by default to 24 hours
    //maxAge: 24 * (60 * 60 * 1000),
    httpOnly: true,
    maxAge: 2 * (60 * 60 * 1000),
    secure: true 
  },
  timezone: 'Europe/Warsaw',
};
